; Start
  !include "MUI2.nsh"
;---------------------------------
;General
 
  Name "Draw Eet Beta"
  OutFile "DrawEetBeta-install.exe"
 
;--------------------------------
;Folder selection page
 
  InstallDir "$PROGRAMFILES\Draw Eet Beta"
  
  InstallDirRegKey HKCU "Software\Draw Eet Beta" ""
  
  RequestExecutionLevel user
 
  Var StartMenuFolder
 
  !define MUI_ABORTWARNING
 
;--------------------------------
;Modern UI Configuration
 
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_DIRECTORY
  
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Draw Eet Beta" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
 
;--------------------------------
;Language
 
  !insertmacro MUI_LANGUAGE "English"
 
 
;-------------------------------- 
;Installer Sections     
Section "Installer Section" SecInstaller
 
;Add files
  SetOutPath "$INSTDIR"
 
  File "DrawEet.exe"
  File "CEGUIBase-0.dll"
  File "CEGUICommonDialogs-0.dll"
  File "CEGUICoreWindowRendererSet.dll"
  File "CEGUIExpatParser.dll"
  File "CEGUIOpenGLRenderer-0.dll"
  File "CEGUISILLYImageCodec.dll"
  File "freetype.dll"
  File "glew.dll"
  File "glfw.dll"
  File "jpeg.dll"
  File "libexpat.dll"
  File "libpng.dll"
  File "pcre.dll"
  File "SDL2.dll"
  File "SDL2_net.dll"
  File "SILLY.dll"
  File "zlib.dll"
  SetOutPath "$INSTDIR\data\fonts"
  File "data\fonts\DejaVuLGCSans.ttf"
  File "data\fonts\DejaVuLGCSans-10.font"
  File "data\fonts\DejaVuLGCSans-12.font"
  File "data\fonts\DejaVuSans.ttf"
  File "data\fonts\DejaVuSans-10.font"
  File "data\fonts\DejaVuSans-12.font"
  SetOutPath "$INSTDIR\data\imagesets"
  File "data\imagesets\DrawEet.imageset"
  File "data\imagesets\DrawEet.png"
  File "data\imagesets\TaharezLook.imageset"
  File "data\imagesets\TaharezLook.png"
  SetOutPath "$INSTDIR\data\layouts"
  File "data\layouts\DrawEet.layout"
  SetOutPath "$INSTDIR\data\looknfeel"
  File "data\looknfeel\DrawEet.looknfeel"
  File "data\looknfeel\Generic.looknfeel"
  File "data\looknfeel\TaharezLook.looknfeel"
  SetOutPath "$INSTDIR\data\schemes"
  File "data\schemes\DrawEet.scheme"
  File "data\schemes\Generic.scheme"
  File "data\schemes\TaharezLook.scheme"
  SetOutPath "$INSTDIR\data\shaders"
  File "data\shaders\capsule.vert"
  File "data\shaders\capsule.geom"
  File "data\shaders\capsule.frag"
  SetOutPath "$INSTDIR\redist"
  File "redist\vcredist_x86.exe"
  
  WriteRegStr HKCU "Software\Draw Eet Beta" "" $INSTDIR
 
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  SetOutPath "$INSTDIR"
  CreateShortCut "$DESKTOP\Draw Eet Beta.lnk" "$INSTDIR\DrawEet.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
	CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Draw Eet Beta.lnk" "$INSTDIR\DrawEet.exe"
	CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "Uninstall"
 
;Delete Files 
  RMDir /r "$INSTDIR\*.*"    
 
;Remove the installation directory
  RMDir "$INSTDIR"
  
  Delete "$DESKTOP\Draw Eet Beta.lnk"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
   
  Delete "$SMPROGRAMS\$StartMenuFolder\Draw Eet Beta.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
 
SectionEnd
 
 
;--------------------------------    
;MessageBox Section
 
 
;Function that calls a messagebox when installation finished correctly
Function .onInstSuccess
  ExecWait "$INSTDIR\redist\vcredist_x86.exe"
FunctionEnd
 
;eof