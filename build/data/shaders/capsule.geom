#version 150

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

in vec4 vsColor[];
out vec4 gsColor;

uniform float gsWidth;

void main()
{
	vec2 p1 = gl_in[0].gl_Position.xy;
	vec2 p2 = gl_in[1].gl_Position.xy;
	
	if (p1 != p2)
	{
		float dist = distance(p1, p2);
		float xToDist = (p2.x - p1.x) / dist;
		float yToDist = (p2.y - p1.y) / dist;
		
		gl_Position = vec4(p1.x - gsWidth * (xToDist + yToDist), p1.y - gsWidth * (yToDist - xToDist), 0.0, 1.0);
		gsColor = vsColor[0];
		EmitVertex();
		
		gl_Position = vec4(p1.x - gsWidth * (xToDist - yToDist), p1.y - gsWidth * (yToDist + xToDist), 0.0, 1.0);
		gsColor = vsColor[0];
		EmitVertex();
		
		gl_Position = vec4(p2.x + gsWidth * (xToDist - yToDist), p2.y + gsWidth * (yToDist + xToDist), 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
		
		gl_Position = vec4(p2.x + gsWidth * (xToDist + yToDist), p2.y + gsWidth * (yToDist - xToDist), 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
	}
	else
	{
		gl_Position = vec4(p1.x - gsWidth, p1.y - gsWidth, 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
		
		gl_Position = vec4(p1.x + gsWidth, p1.y - gsWidth, 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
		
		gl_Position = vec4(p1.x - gsWidth, p1.y + gsWidth, 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
		
		gl_Position = vec4(p1.x + gsWidth, p1.y + gsWidth, 0.0, 1.0);
		gsColor = vsColor[1];
		EmitVertex();
	}
}
