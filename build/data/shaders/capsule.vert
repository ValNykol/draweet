#version 150

in vec4 inPosition;
in vec4 inColor;

out vec4 vsColor;

void main()
{
	vsColor = inColor;
	gl_Position = inPosition;
}
