#version 150

in vec4 gsColor;
out vec4 fsColor;

uniform vec2 segment[2];
uniform float fsWidth;

//const float feathering = 0.1;

float distToCapsule(vec2 p, int a, int b)
{
	vec2 ba = segment[b] - segment[a];
	vec2 pa = p - segment[a];
	
	float t = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
	return distance(pa, ba * t);
}

void main()
{
	float dist = distToCapsule(gl_FragCoord.xy, 0, 1);
	
	float featherDist = 1.5;
	float solidDist = fsWidth - featherDist;
	
	float alpha = (featherDist - clamp(dist - solidDist, 0.0, featherDist)) / featherDist;
	fsColor = vec4(gsColor.rgb, alpha);
}
