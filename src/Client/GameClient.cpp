#define NOMINMAX

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

#include "SDL.h"
#include "SDL_net.h"

#include "GL/glew.h"

#include "CEGUI/BasicImage.h"
#include "CEGUI/DefaultResourceProvider.h"
#include "CEGUI/FontManager.h"
#include "CEGUI/GUIContext.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/TypedProperty.h"
#include "CEGUI/SchemeManager.h"
#include "CEGUI/ScriptModule.h"
#include "CEGUI/System.h"
#include "CEGUI/Window.h"
#include "CEGUI/WindowManager.h"
#include "CEGUI/XMLParser.h"
#include "CEGUI/falagard/WidgetLookManager.h"
#include "CEGUI/RendererModules/OpenGL/GL3Renderer.h"
#include "CEGUI/RendererModules/OpenGL/Texture.h"
#include "CEGUI/widgets/Editbox.h"
#include "CEGUI/widgets/FrameWindow.h"
#include "CEGUI/widgets/ListBox.h"
#include "CEGUI/widgets/ListboxTextItem.h"
#include "CEGUI/widgets/PushButton.h"
#include "CEGUI/widgets/Scrollbar.h"
#include "CEGUI/WindowRendererSets/Core/StaticImage.h"

#include "Core/MacroUtils.h"
#include "Core/NetworkHelper.h"
#include "CapsuleShader.h"

#define WEIRD_THING 0
#define WEIRD_THING_USE_ALPHA 0

#define WEIRD_THING_CHANGE_BRUSH 0

static const unsigned	BUTTER_INTERVAL = 300;
static const unsigned	BUTTER_CHUNKS = 10;
static const float		DEFAULT_BRUSH_SIZE = 10.0f;
static const float		MIN_BRUSH_SIZE = 3.0f;
static const float		MAX_BRUSH_SIZE = 60.0f;
static const unsigned	BRUSH_PREVIEW_SIZE = 160;
static const float		MIN_FEATHER = 1.5f;

static const float		CHAT_HISTORY = 100;

static const unsigned	FAKE_VSYNC_DELAY = 15;

struct GameClient : MessageReceiver
{
	GameClient();

	void InitKeymap();
	bool InitSDL();
	void InitCEGUI();
	void InitCanvas();

	void Update();

	bool InitSDLNet();

	bool Handle_CanvasMouseMove			(const CEGUI::EventArgs& e);
	bool Handle_CanvasMouseDown			(const CEGUI::EventArgs& e);
	bool Handle_CanvasMouseUp			(const CEGUI::EventArgs& e);
	bool Handle_CanvasMouseLeave		(const CEGUI::EventArgs& e);
	bool Handle_CanvasMouseEnter		(const CEGUI::EventArgs& e);
	bool Handle_CanvasMouseWheel		(const CEGUI::EventArgs& e);

	bool Handle_ConnectButtonClicked	(const CEGUI::EventArgs& e);
	bool Handle_ConnectKeyDown			(const CEGUI::EventArgs& e);
	bool Handle_CanvasScaleScrolled		(const CEGUI::EventArgs& e);

	bool Handle_ChatEditKeyDown			(const CEGUI::EventArgs& e);
	bool Handle_GuessEditKeyDown		(const CEGUI::EventArgs& e);

	bool Handle_ColorClicked			(const CEGUI::EventArgs& e);
	bool Handle_ClearButtonClicked		(const CEGUI::EventArgs& e);

	void Connect();

	void CanvasDrawChunk(CEGUI::Vector2f newPos);
	void DrawBrushPreview();

	void ShowChatMessage(CEGUI::String message);

	void UpdatePlayersData();

	// MessageReceiver
	virtual void OnMessageReceivedServerFull();
	virtual void OnMessageReceivedJoinSuccess(uint8_t myIndex);
	virtual void OnMessageReceivedClientsData(const ClientsData& clientsData);
	virtual void OnMessageReceivedPassButter(const PassButter& passButter);
	virtual void OnMessageReceivedTurnEnd(const std::string& word);

	virtual void OnMessageReceivedButter(const Butter& butter, uint8_t senderIndex);
	virtual void OnMessageReceivedChat(const std::string& message, uint8_t senderIndex);
	virtual void OnMessageReceivedGuess(const Guess& guess, uint8_t senderIndex);

	std::map<SDL_Scancode, CEGUI::Key::Scan> m_Keymap;

	int							m_WindowWidth;
	int							m_WindowHeight;
	SDL_Window*					m_SDLWindow;
	CEGUI::Window*				m_RootWindow;

	CEGUI::Texture*				m_CanvasTexture;
	uint32_t*					m_CanvasBuffer;
	Framebuffer					m_CanvasGLBuffer;
	CEGUI::Rectf				m_CanvasArea;
	CEGUI::Size<int>			m_CanvasSize;
	float						m_CanvasScale;

	CEGUI::Texture*				m_BrushPreviewTexture;
	uint32_t*					m_BrushPreviewBuffer;
	Framebuffer					m_BrushPreviewGLBuffer;
	CEGUI::Rectf				m_BrushPreviewArea;
	bool						m_ShouldRedrawBrushPreview;

	CapsuleShader				m_CapsuleShader;

	bool						m_IsDrawing;
	CEGUI::Vector2f				m_OldMousePos;
	float						m_AccumulatedMovement;
	CEGUI::Vector2f				m_OldBrushPos;
	CEGUI::Colour				m_OldBrushColor;

	bool						m_Running;
	double						m_LastTimePulse;

	bool						m_Vsync;
#if WEIRD_THING
	CEGUI::ColourRect			m_CanvasColorRect;
#if WEIRD_THING_USE_ALPHA
	signed char					m_CanvasColorRectChanging[4][4];
#else
	signed char					m_CanvasColorRectChanging[4][3];
#endif
#endif
	float						m_CanvasBrushSize;
	float						m_CanvasBrushHardness;
	CEGUI::Colour				m_CanvasBrushColor;

	bool						m_CanvasEraser;
#if WEIRD_THING_CHANGE_BRUSH
	signed char					m_CanvasBrushColorChanging[4];
#endif

	uint8_t						m_MyIndex;
	std::string					m_MyName;
	ClientsData					m_PlayersData;
	std::vector<uint8_t>		m_PlayerOrder;
	std::map<uint8_t, bool>		m_GuessedThisTurn;

	Butter						m_Butter;
	unsigned					m_ButterTime;

	PassButter					m_PassButter;
	unsigned					m_TurnStart;

	// Network stuff
	bool						m_ConnectingToServer;

	TCPsocket					m_ClientSocket;
	SDLNet_SocketSet			m_SocketSet;
	char						m_InBuffer[BUFFER_SIZE];
	unsigned					m_InBufferOffset;
	unsigned					m_InMessageSize;
	char						m_OutBuffer[BUFFER_SIZE];
	unsigned					m_OutBufferOffset;

	char						m_PacketBuffer[PACKET_SIZE];
};

static GameClient m_GameClient;

void GameClient::OnMessageReceivedServerFull()
{
	m_Running = false;
}

void GameClient::OnMessageReceivedJoinSuccess(uint8_t myIndex)
{
	m_ConnectingToServer = false;

	m_MyIndex = myIndex;

	ClientInfo myInfo;
	myInfo.Name = m_MyName;
	m_PlayersData.insert(ClientIndexInfo(myIndex, myInfo));

	UpdatePlayersData();

	m_RootWindow->getChild("CanvasWindow")->show();
	m_RootWindow->getChild("ChatEdit")->show();
	m_RootWindow->getChild("ChatLabel")->show();
	m_RootWindow->getChild("ChatBox")->show();
	m_RootWindow->getChild("GuessBox")->show();
	m_RootWindow->getChild("PlayerBox")->show();
	m_RootWindow->getChild("WordBox")->show();

	m_RootWindow->getChild("PlayerNameLabel")->hide();
	m_RootWindow->getChild("ServerNameLabel")->hide();
	m_RootWindow->getChild("PlayerNameEdit")->hide();
	m_RootWindow->getChild("ServerNameEdit")->hide();
	m_RootWindow->getChild("ConnectButton")->hide();
	m_RootWindow->getChild("CanvasScaleLabel")->hide();
	m_RootWindow->getChild("CanvasScaleScrollbar")->hide();
	m_RootWindow->getChild("CanvasScaleText")->hide();

	char* buffer = m_OutBuffer + m_OutBufferOffset;
	unsigned bufferSize = sizeof(m_OutBuffer) - m_OutBufferOffset;

	m_OutBufferOffset += GenerateMessageClientInfo(buffer, bufferSize, myInfo);
}

void GameClient::OnMessageReceivedClientsData(const ClientsData& clientsData)
{
	using namespace CEGUI;

	for (auto& clientIndexInfo : clientsData)
	{
		if (m_PlayersData.find(clientIndexInfo.first) != m_PlayersData.end())
		{
			auto& playerInfo = m_PlayersData.at(clientIndexInfo.first);

			if (clientIndexInfo.second.Name.empty() && clientIndexInfo.second.Points == 0)
			{
				String messageToShow = "Player [" + playerInfo.Name + "] has disconnected.";
				ShowChatMessage(messageToShow);

				m_PlayersData.erase(clientIndexInfo.first);
			}
			else
			{
				int delta = clientIndexInfo.second.Points - playerInfo.Points;

				playerInfo.Points += delta;

				if (clientIndexInfo.first != m_PassButter.DrawingPlayer)
				{
					m_GuessedThisTurn.insert(std::pair<uint8_t, bool>(clientIndexInfo.first, true));
				}
			}
		}
		else if (!clientIndexInfo.second.Name.empty())
		{
			m_PlayersData.insert(clientIndexInfo);

			String messageToShow = "Player [" + clientIndexInfo.second.Name + "] has joined the game.";
			ShowChatMessage(messageToShow);
		}
	}

	UpdatePlayersData();
}

void GameClient::OnMessageReceivedPassButter(const PassButter& passButter)
{
	using namespace CEGUI;

	m_PassButter = passButter;

	m_TurnStart = SDL_GetTicks() / 1000;

	//memset((void*)m_CanvasBuffer, 0, m_CanvasSize.d_width * m_CanvasSize.d_height * sizeof(*m_CanvasBuffer));
	//m_CanvasTexture->blitFromMemory(m_CanvasBuffer, m_CanvasArea);
	m_CanvasGLBuffer.Clear();

	m_CanvasBrushColor = 0;
	m_CanvasBrushSize = DEFAULT_BRUSH_SIZE;
	m_CanvasEraser = false;

	m_Butter.clear();
	m_IsDrawing = false;

	m_RootWindow->getChild("CanvasLabel")->show();
	m_RootWindow->getChild("TurnLabel")->show();
	m_RootWindow->getChild("TimeLabel")->show();
	m_RootWindow->getChild("TimeText")->show();

	Listbox* guessBox = static_cast<Listbox*>(m_RootWindow->getChild("GuessBox"));
	while (guessBox->getItemCount() > 0)
	{
		guessBox->removeItem(guessBox->getListboxItemFromIndex(0));
	}

	Window* canvasWindow = m_RootWindow->getChild("CanvasWindow");

	switch (passButter.Type)
	{
	case PBPass:
		{
			const Image* defaultCursor = System::getSingleton().getDefaultGUIContext().getMouseCursor().getDefaultImage();
			canvasWindow->setMouseCursor(defaultCursor);

			m_RootWindow->getChild("GuessEdit")->show();

			m_RootWindow->getChild("ClearButton")->hide();
			m_RootWindow->getChild("ColorBox")->hide();
			m_RootWindow->getChild("GuessLabel")->hide();

			m_RootWindow->getChild("GuessEdit")->enable();
			m_RootWindow->getChild("GuessEdit")->setText("");

			String drawingText;
			if (m_PlayersData.find(passButter.DrawingPlayer) != m_PlayersData.end())
			{
				drawingText = m_PlayersData.at(passButter.DrawingPlayer).Name;
			}
			else
			{
				drawingText = "I am error";
			}

			drawingText += " is drawing";

			if (passButter.DrawingPlayer == 0xFF)
			{
				m_RootWindow->getChild("GuessEdit")->hide();
				m_RootWindow->getChild("GuessLabel")->hide();
				m_RootWindow->getChild("CanvasWindow")->hide();
				m_RootWindow->getChild("GuessBox")->hide();
				m_RootWindow->getChild("TurnLabel")->hide();
				m_RootWindow->getChild("TimeLabel")->hide();
				m_RootWindow->getChild("TimeText")->hide();
				drawingText = "The game is kill!";

				m_RootWindow->getChild("WinLabel")->show();
				String winText = "A WINNER IS ";
				uint8_t winner = m_MyIndex;
				for (auto& playerIndexInfo : m_PlayersData)
				{
					auto& playerInfo = playerIndexInfo.second;
					if (playerInfo.Points > m_PlayersData[winner].Points)
					{
						winner = playerIndexInfo.first;
					}
				}

				if (winner == m_MyIndex)
				{
					winText += "YOU";
				}
				else
				{
					winText += m_PlayersData[winner].Name;
				}

				m_RootWindow->getChild("WinLabel")->setText(winText);
			}

			m_RootWindow->getChild("CanvasLabel")->setText(drawingText);
		} break;
	case PBMake:
		{
			m_PassButter.DrawingPlayer = m_MyIndex;

			m_ShouldRedrawBrushPreview = true;
			canvasWindow->setMouseCursor("DrawEetLook/BrushPreview");

			m_RootWindow->getChild("ClearButton")->show();
			m_RootWindow->getChild("ColorBox")->show();

			m_RootWindow->getChild("GuessLabel")->show();

			m_RootWindow->getChild("GuessEdit")->hide();

			String textToShow = "The word is \"" + passButter.Word + "\"";
			m_RootWindow->getChild("GuessLabel")->setText(textToShow);

			m_RootWindow->getChild("CanvasLabel")->setText("You are drawing");
		} break;
	}

	m_GuessedThisTurn.clear();
	UpdatePlayersData();

	String turnsRemaining;
	if (passButter.TurnsLeft > 0)
	{
		char turnsText[3];
		ReturnIf(passButter.TurnsLeft + 1 >= 100);
		sprintf(turnsText, "%d", passButter.TurnsLeft + 1);

		turnsRemaining = turnsText;
		turnsRemaining += " turns remaining!";
	}
	else
	{
		turnsRemaining = "The last turn!";
	}

	m_RootWindow->getChild("TurnLabel")->setText(turnsRemaining);
}

void GameClient::OnMessageReceivedTurnEnd(const std::string& word)
{
	ReturnIf(m_PassButter.Type == PBWait);

	using namespace CEGUI;

	uint8_t drawingPlayer = (m_PassButter.Type == PBPass) ? m_PassButter.DrawingPlayer : m_MyIndex;

	std::string utf8MessageToShow = "[" + m_PlayersData.find(m_PassButter.DrawingPlayer)->second.Name + "] was drawing \"" + word + "\"";
	String messageToShow = (uint8_t*)utf8MessageToShow.c_str();
	ListboxTextItem* wordBoxItem = new ListboxTextItem(messageToShow);
	wordBoxItem->setTextParsingEnabled(false);
	static_cast<Listbox*>(m_RootWindow->getChild("WordBox"))->insertItem(wordBoxItem, 0);
}

void GameClient::OnMessageReceivedButter(const Butter& butter, uint8_t senderIndex)
{
	ReturnUnless(m_PassButter.Type == PBPass);
	ReturnUnless(senderIndex == m_PassButter.DrawingPlayer);

	m_Butter.insert(m_Butter.end(), butter.begin(), butter.end());
}

void GameClient::OnMessageReceivedChat(const std::string& message, uint8_t senderIndex)
{
	ReturnIf(m_PlayersData.find(senderIndex) == m_PlayersData.end());

	using namespace CEGUI;
	std::string utf8MessageToShow = "[" + m_PlayersData.find(senderIndex)->second.Name + "]: " + message;
	String messageToShow = (uint8_t*)utf8MessageToShow.c_str();
	ShowChatMessage(messageToShow);
}

void GameClient::OnMessageReceivedGuess(const Guess& guess, uint8_t senderIndex)
{
	using namespace CEGUI;

	switch (guess.Type)
	{
	case GSuccess:
		{
			m_RootWindow->getChild("GuessLabel")->show();
			m_RootWindow->getChild("GuessEdit")->hide();

			String textToShow = "Correct! The word was \"" + guess.Word + "\"";
			m_RootWindow->getChild("GuessLabel")->setText(textToShow);
		} break;
	case GAttempt:
		{
			ReturnIf(m_PlayersData.find(senderIndex) == m_PlayersData.end());

			String guessToShow = "[" + m_PlayersData.find(senderIndex)->second.Name + "]: " + guess.Word;
			ListboxTextItem* guessBoxItem = new ListboxTextItem(guessToShow);
			guessBoxItem->setTextParsingEnabled(false);
			static_cast<Listbox*>(m_RootWindow->getChild("GuessBox"))->insertItem(guessBoxItem, 0);

			if (senderIndex == m_MyIndex)
			{
				m_RootWindow->getChild("GuessEdit")->enable();
			}
		} break;
	}
}

GameClient::GameClient()
 : m_SDLWindow(nullptr)
 , m_Running(true)
 , m_LastTimePulse(0.0)
 , m_WindowWidth(1280)
 , m_WindowHeight(720)
 , m_RootWindow(nullptr)
 , m_CanvasTexture(nullptr)
 , m_CanvasBuffer(nullptr)
 , m_IsDrawing(false)
 , m_AccumulatedMovement(0.0f)
 , m_CanvasBrushHardness(1.0f)
 , m_CanvasScale(1.0f)
#if WEIRD_THING
 , m_CanvasColorRect(CEGUI::Colour(1.0f, 1.0f, 1.0f))
#endif
 , m_CanvasBrushColor(0.0f, 0.0f, 0.0f)
 , m_ConnectingToServer(false)
 , m_ClientSocket(nullptr)
 , m_SocketSet(nullptr)
{
#if WEIRD_THING
#if WEIRD_THING_USE_ALPHA
	memset(m_CanvasColorRectChanging, 0, sizeof(m_CanvasColorRectChanging[0][0]) * 4 * 4);
#else
	memset(m_CanvasColorRectChanging, 0, sizeof(m_CanvasColorRectChanging[0][0]) * 4 * 3);
#endif
#endif
#if WEIRD_THING_CHANGE_BRUSH
	memset(m_CanvasBrushColorChanging, 0, sizeof(m_CanvasBrushColorChanging[0]) * 4);
#endif
}

void GameClient::InitKeymap()
{
	m_Keymap[SDL_SCANCODE_1]			= CEGUI::Key::One;
	m_Keymap[SDL_SCANCODE_2]			= CEGUI::Key::Two;
	m_Keymap[SDL_SCANCODE_3]			= CEGUI::Key::Three;
	m_Keymap[SDL_SCANCODE_4]			= CEGUI::Key::Four;
	m_Keymap[SDL_SCANCODE_5]			= CEGUI::Key::Five;
	m_Keymap[SDL_SCANCODE_6]			= CEGUI::Key::Six;
	m_Keymap[SDL_SCANCODE_7]			= CEGUI::Key::Seven;
	m_Keymap[SDL_SCANCODE_8]			= CEGUI::Key::Eight;
	m_Keymap[SDL_SCANCODE_9]			= CEGUI::Key::Nine;
	m_Keymap[SDL_SCANCODE_0]			= CEGUI::Key::Zero;
	  
	m_Keymap[SDL_SCANCODE_Q]			= CEGUI::Key::Q;
	m_Keymap[SDL_SCANCODE_W]			= CEGUI::Key::W;
	m_Keymap[SDL_SCANCODE_E]			= CEGUI::Key::E;
	m_Keymap[SDL_SCANCODE_R]			= CEGUI::Key::R;
	m_Keymap[SDL_SCANCODE_T]			= CEGUI::Key::T;
	m_Keymap[SDL_SCANCODE_Y]			= CEGUI::Key::Y;
	m_Keymap[SDL_SCANCODE_U]			= CEGUI::Key::U;
	m_Keymap[SDL_SCANCODE_I]			= CEGUI::Key::I;
	m_Keymap[SDL_SCANCODE_O]			= CEGUI::Key::O;
	m_Keymap[SDL_SCANCODE_P]			= CEGUI::Key::P;
	m_Keymap[SDL_SCANCODE_A]			= CEGUI::Key::A;
	m_Keymap[SDL_SCANCODE_S]			= CEGUI::Key::S;
	m_Keymap[SDL_SCANCODE_D]			= CEGUI::Key::D;
	m_Keymap[SDL_SCANCODE_F]			= CEGUI::Key::F;
	m_Keymap[SDL_SCANCODE_G]			= CEGUI::Key::G;
	m_Keymap[SDL_SCANCODE_H]			= CEGUI::Key::H;
	m_Keymap[SDL_SCANCODE_J]			= CEGUI::Key::J;
	m_Keymap[SDL_SCANCODE_K]			= CEGUI::Key::K;
	m_Keymap[SDL_SCANCODE_L]			= CEGUI::Key::L;
	m_Keymap[SDL_SCANCODE_Z]			= CEGUI::Key::Z;
	m_Keymap[SDL_SCANCODE_X]			= CEGUI::Key::X;
	m_Keymap[SDL_SCANCODE_C]			= CEGUI::Key::C;
	m_Keymap[SDL_SCANCODE_V]			= CEGUI::Key::V;
	m_Keymap[SDL_SCANCODE_B]			= CEGUI::Key::B;
	m_Keymap[SDL_SCANCODE_N]			= CEGUI::Key::N;
	m_Keymap[SDL_SCANCODE_M]			= CEGUI::Key::M;
	  
	m_Keymap[SDL_SCANCODE_COMMA]		= CEGUI::Key::Comma;
	m_Keymap[SDL_SCANCODE_PERIOD]		= CEGUI::Key::Period;
	m_Keymap[SDL_SCANCODE_SLASH]		= CEGUI::Key::Slash;
	m_Keymap[SDL_SCANCODE_BACKSLASH]	= CEGUI::Key::Backslash;
	m_Keymap[SDL_SCANCODE_MINUS]		= CEGUI::Key::Minus;
	m_Keymap[SDL_SCANCODE_EQUALS]		= CEGUI::Key::Equals;
	m_Keymap[SDL_SCANCODE_SEMICOLON]	= CEGUI::Key::Semicolon;
	m_Keymap[SDL_SCANCODE_LEFTBRACKET]	= CEGUI::Key::LeftBracket;
	m_Keymap[SDL_SCANCODE_RIGHTBRACKET]	= CEGUI::Key::RightBracket;
	m_Keymap[SDL_SCANCODE_APOSTROPHE]	= CEGUI::Key::Apostrophe;
	m_Keymap[SDL_SCANCODE_GRAVE]		= CEGUI::Key::Grave;
	  
	m_Keymap[SDL_SCANCODE_RETURN]		= CEGUI::Key::Return;
	m_Keymap[SDL_SCANCODE_SPACE]		= CEGUI::Key::Space;
	m_Keymap[SDL_SCANCODE_BACKSPACE]	= CEGUI::Key::Backspace;
	m_Keymap[SDL_SCANCODE_TAB]			= CEGUI::Key::Tab;
	  
	m_Keymap[SDL_SCANCODE_ESCAPE]		= CEGUI::Key::Escape;
	m_Keymap[SDL_SCANCODE_PAUSE]		= CEGUI::Key::Pause;
	m_Keymap[SDL_SCANCODE_SYSREQ]		= CEGUI::Key::SysRq;
	m_Keymap[SDL_SCANCODE_POWER]		= CEGUI::Key::Power;
	  
	m_Keymap[SDL_SCANCODE_NUMLOCKCLEAR]	= CEGUI::Key::NumLock;
	m_Keymap[SDL_SCANCODE_SCROLLLOCK]	= CEGUI::Key::ScrollLock;
	  
	m_Keymap[SDL_SCANCODE_F1]			= CEGUI::Key::F1;
	m_Keymap[SDL_SCANCODE_F2]			= CEGUI::Key::F2;
	m_Keymap[SDL_SCANCODE_F3]			= CEGUI::Key::F3;
	m_Keymap[SDL_SCANCODE_F4]			= CEGUI::Key::F4;
	m_Keymap[SDL_SCANCODE_F5]			= CEGUI::Key::F5;
	m_Keymap[SDL_SCANCODE_F6]			= CEGUI::Key::F6;
	m_Keymap[SDL_SCANCODE_F7]			= CEGUI::Key::F7;
	m_Keymap[SDL_SCANCODE_F8]			= CEGUI::Key::F8;
	m_Keymap[SDL_SCANCODE_F9]			= CEGUI::Key::F9;
	m_Keymap[SDL_SCANCODE_F10]			= CEGUI::Key::F10;
	m_Keymap[SDL_SCANCODE_F11]			= CEGUI::Key::F11;
	m_Keymap[SDL_SCANCODE_F12]			= CEGUI::Key::F12;
	m_Keymap[SDL_SCANCODE_F13]			= CEGUI::Key::F13;
	m_Keymap[SDL_SCANCODE_F14]			= CEGUI::Key::F14;
	m_Keymap[SDL_SCANCODE_F15]			= CEGUI::Key::F15;
	  
	m_Keymap[SDL_SCANCODE_LCTRL]		= CEGUI::Key::LeftControl;
	m_Keymap[SDL_SCANCODE_LALT]			= CEGUI::Key::LeftAlt;
	m_Keymap[SDL_SCANCODE_LSHIFT]		= CEGUI::Key::LeftShift;
	m_Keymap[SDL_SCANCODE_LGUI]			= CEGUI::Key::LeftWindows;
	m_Keymap[SDL_SCANCODE_RCTRL]		= CEGUI::Key::RightControl;
	m_Keymap[SDL_SCANCODE_RALT]			= CEGUI::Key::RightAlt;
	m_Keymap[SDL_SCANCODE_RSHIFT]		= CEGUI::Key::RightShift;
	m_Keymap[SDL_SCANCODE_RGUI]			= CEGUI::Key::RightWindows;
	m_Keymap[SDL_SCANCODE_MENU]			= CEGUI::Key::AppMenu;
	  
	m_Keymap[SDL_SCANCODE_KP_0]			= CEGUI::Key::Numpad0;
	m_Keymap[SDL_SCANCODE_KP_1]			= CEGUI::Key::Numpad1;
	m_Keymap[SDL_SCANCODE_KP_2]			= CEGUI::Key::Numpad2;
	m_Keymap[SDL_SCANCODE_KP_3]			= CEGUI::Key::Numpad3;
	m_Keymap[SDL_SCANCODE_KP_4]			= CEGUI::Key::Numpad4;
	m_Keymap[SDL_SCANCODE_KP_5]			= CEGUI::Key::Numpad5;
	m_Keymap[SDL_SCANCODE_KP_6]			= CEGUI::Key::Numpad6;
	m_Keymap[SDL_SCANCODE_KP_7]			= CEGUI::Key::Numpad7;
	m_Keymap[SDL_SCANCODE_KP_8]			= CEGUI::Key::Numpad8;
	m_Keymap[SDL_SCANCODE_KP_9]			= CEGUI::Key::Numpad9;
	m_Keymap[SDL_SCANCODE_KP_PERIOD]	= CEGUI::Key::Decimal;
	m_Keymap[SDL_SCANCODE_KP_PLUS]		= CEGUI::Key::Add;
	m_Keymap[SDL_SCANCODE_KP_MINUS]		= CEGUI::Key::Subtract;
	m_Keymap[SDL_SCANCODE_KP_MULTIPLY]	= CEGUI::Key::Multiply;
	m_Keymap[SDL_SCANCODE_KP_DIVIDE]	= CEGUI::Key::Divide;
	m_Keymap[SDL_SCANCODE_KP_ENTER]		= CEGUI::Key::NumpadEnter;
	  
	m_Keymap[SDL_SCANCODE_UP]			= CEGUI::Key::ArrowUp;
	m_Keymap[SDL_SCANCODE_LEFT]			= CEGUI::Key::ArrowLeft;
	m_Keymap[SDL_SCANCODE_RIGHT]		= CEGUI::Key::ArrowRight;
	m_Keymap[SDL_SCANCODE_DOWN]			= CEGUI::Key::ArrowDown;
	  
	m_Keymap[SDL_SCANCODE_HOME]			= CEGUI::Key::Home;
	m_Keymap[SDL_SCANCODE_END]			= CEGUI::Key::End;
	m_Keymap[SDL_SCANCODE_PAGEUP]		= CEGUI::Key::PageUp;
	m_Keymap[SDL_SCANCODE_PAGEDOWN]		= CEGUI::Key::PageDown;
	m_Keymap[SDL_SCANCODE_INSERT]		= CEGUI::Key::Insert;
	m_Keymap[SDL_SCANCODE_DELETE]		= CEGUI::Key::Delete;
}

bool GameClient::InitSDL()
{
	srand((unsigned)time(0));

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

	m_SDLWindow = SDL_CreateWindow("DrawEet", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_WindowWidth, m_WindowHeight, SDL_WINDOW_OPENGL);
	if (!m_SDLWindow)
	{
		fprintf(stderr, "Could not create the window: %s\n", SDL_GetError());
		return false;
	}

	SDL_GLContext context = SDL_GL_CreateContext(m_SDLWindow);
	if (!context)
	{
		fprintf(stderr, "Could not create GL context: %s\n", SDL_GetError());
		return false;
	}

	if (SDL_GL_SetSwapInterval(1) < 0)
	{
		m_Vsync = false;
		fprintf(stderr, "Could not set VSync: %s\n", SDL_GetError());
	}
	else
	{
		m_Vsync = true;
	}

	SDL_StartTextInput();
	SDL_ShowCursor(SDL_DISABLE);

	return true;
}

bool GameClient::InitSDLNet()
{
	ReturnIf(m_ConnectingToServer, false);

	IPaddress serverIP;

	m_MyName = m_RootWindow->getChild("PlayerNameEdit")->getText().c_str();
	ReturnIf(m_MyName.empty(), false);

	std::string serverName = m_RootWindow->getChild("ServerNameEdit")->getText().c_str();
	ReturnIf(serverName.empty(), false);

	m_ConnectingToServer = true;

	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		return false;
	}

	if (SDLNet_ResolveHost(&serverIP, serverName.c_str(), 1337) < 0)
	{
		fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		return false;
	}

	if (!(m_ClientSocket = SDLNet_TCP_Open(&serverIP)))
	{
		fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		return false;
	}

	m_SocketSet = SDLNet_AllocSocketSet(1);
	SDLNet_TCP_AddSocket(m_SocketSet, m_ClientSocket);

	{
		uint8_t* address = (uint8_t*)&serverIP.host;
		printf("Server IP: %d.%d.%d.%d\n", address[0], address[1], address[2], address[3]);
	}

	// This seems to be really slow O_o
	/*const char* hostName = SDLNet_ResolveIP(&serverIP);
	if (hostName)
	{
		printf("Server host name: %s\n", hostName);
	}*/

	return true;
}

void GameClient::InitCEGUI()
{
	using namespace CEGUI;

	InitKeymap();

	OpenGL3Renderer& renderer = OpenGL3Renderer::bootstrapSystem(Sizef((float)m_WindowWidth, (float)m_WindowHeight));

	{
		DefaultResourceProvider* resourceProvider = static_cast<DefaultResourceProvider*>(System::getSingleton().getResourceProvider());

		resourceProvider->setResourceGroupDirectory("fonts",		"data/fonts/");
		resourceProvider->setResourceGroupDirectory("imagesets",	"data/imagesets/");
		resourceProvider->setResourceGroupDirectory("layouts",		"data/layouts/");
		resourceProvider->setResourceGroupDirectory("looknfeels",	"data/looknfeel/");
		resourceProvider->setResourceGroupDirectory("lua_scripts",	"data/lua_scripts/");
		resourceProvider->setResourceGroupDirectory("schemes",		"data/schemes/");

		Font::setDefaultResourceGroup("fonts");
		ImageManager::setImagesetDefaultResourceGroup("imagesets");
		WindowManager::setDefaultResourceGroup("layouts");
		WidgetLookManager::setDefaultResourceGroup("looknfeels");
		ScriptModule::setDefaultResourceGroup("lua_scripts");
		Scheme::setDefaultResourceGroup("schemes");
	}

	SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
	SchemeManager::getSingleton().createFromFile("Generic.scheme");
	SchemeManager::getSingleton().createFromFile("DrawEet.scheme");
	FontManager::getSingleton().createFromFile("DejaVuLGCSans-10.font");

	m_CapsuleShader.Init("data/shaders/capsule.vert", "data/shaders/capsule.geom", "data/shaders/capsule.frag");

	WindowManager& windowManager = WindowManager::getSingleton();

	m_RootWindow = windowManager.loadLayoutFromFile("DrawEet.layout");

	{
		float brushPreviewSizef = (float)BRUSH_PREVIEW_SIZE;
		float halfBrushPreviewSizef = brushPreviewSizef / 2.0f;
		m_BrushPreviewTexture = &renderer.createTexture("BrushPreviewTexture", Sizef(brushPreviewSizef, brushPreviewSizef));
		
		BasicImage* brushPreviewImage = static_cast<BasicImage*>(&ImageManager::getSingleton().create("BasicImage", "DrawEetLook/BrushPreview"));

		m_BrushPreviewArea = Rectf(0.0f, 0.0f, brushPreviewSizef, brushPreviewSizef);

		brushPreviewImage->setArea(m_BrushPreviewArea);
		brushPreviewImage->setAutoScaled(ASM_Disabled);
		brushPreviewImage->setOffset(Vector2f(-halfBrushPreviewSizef, -halfBrushPreviewSizef));

		brushPreviewImage->setTexture(m_BrushPreviewTexture);

		m_BrushPreviewGLBuffer.Init(BRUSH_PREVIEW_SIZE, BRUSH_PREVIEW_SIZE, static_cast<OpenGLTexture*>(m_BrushPreviewTexture)->getOpenGLTexture());
		//m_BrushPreviewBuffer = static_cast<uint32_t*>(calloc(BRUSH_PREVIEW_SIZE * BRUSH_PREVIEW_SIZE, sizeof(*m_BrushPreviewBuffer)));
	}

	{
		GUIContext& context = System::getSingleton().getDefaultGUIContext();
		context.setRootWindow(m_RootWindow);
		context.setDefaultFont("DejaVuLGCSans-10");
		context.getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	}

	{
		// Hide all the stuff
		m_RootWindow->getChild("CanvasWindow")->hide();
		m_RootWindow->getChild("ChatEdit")->hide();
		m_RootWindow->getChild("ChatLabel")->hide();
		m_RootWindow->getChild("ChatBox")->hide();
		m_RootWindow->getChild("CanvasLabel")->hide();
		m_RootWindow->getChild("GuessEdit")->hide();
		m_RootWindow->getChild("GuessBox")->hide();
		m_RootWindow->getChild("PlayerBox")->hide();
		m_RootWindow->getChild("TurnLabel")->hide();
		m_RootWindow->getChild("GuessLabel")->hide();
		m_RootWindow->getChild("ClearButton")->hide();
		m_RootWindow->getChild("TimeLabel")->hide();
		m_RootWindow->getChild("TimeText")->hide();
		m_RootWindow->getChild("WordBox")->hide();

		Window* colorBox = m_RootWindow->getChild("ColorBox");
		colorBox->hide();

		Window* selection = colorBox->getChild("Selection");
		Sizef selectionSizef = selection->getPixelSize();
		USize selectionUSize = selection->getSize();
		selectionUSize = USize(selectionUSize.d_width, UDim(0.0f, selectionSizef.d_height));
		Vector2f selectionPosf = selection->getPixelPosition() - colorBox->getPixelPosition();
		float itemXOffset = selectionPosf.d_x + 2.0f;
		float itemYOffset = selectionPosf.d_y + 2.0f;

		float itemDim = selectionSizef.d_height - 4.0f;
		USize itemSize = USize(UDim(0.0f, itemDim), UDim(0.0f, itemDim));

		std::vector<Colour> colors;

		{
			colors.push_back(0xFF000000);
			colors.push_back(0xFFFFFFFF);
			colors.push_back(0xFF999999);
			colors.push_back(0xFF333333);
			colors.push_back(0xFF0000FF);
			colors.push_back(0xFF00FFFF);
			colors.push_back(0xFF00FF00);
			colors.push_back(0xFFFFFF00);
			colors.push_back(0xFFFF0000);
			colors.push_back(0xFFFF00FF);
			colors.push_back(0xFFFFC0CB);
			colors.push_back(0xFF660066);
			colors.push_back(0xFFFFA500);
			colors.push_back(0xFF604010);
			colors.push_back(0xFF006400);
			colors.push_back(0xFF71C671);
			colors.push_back(0xFF8E8E38);
			colors.push_back(0xFFC67171);
		}

		for (unsigned i = 0; i < colors.size(); ++i)
		{
			char letter = 'A' + i;
			String optionName = "Option";
			optionName += letter;
			Window* colorOption = WindowManager::getSingleton().createWindow("Generic/Image", optionName);
			colorOption->setProperty<Image*>("Image", &ImageManager::getSingleton().get("TaharezLook/GenericBrush"));
			colorOption->setProperty<ColourRect>("ImageColours", ColourRect(colors[i]));

			colorOption->setSize(itemSize);
			colorOption->setPosition(UVector2(UDim(0.0f, itemXOffset + (itemXOffset + itemDim) * i), UDim(0.0f, itemYOffset)));

			colorOption->subscribeEvent(Window::EventMouseClick, Event::Subscriber(&GameClient::Handle_ColorClicked, this));

			colorBox->addChild(colorOption);
		}
	}

	{
		m_RootWindow->getChild("ConnectButton")->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GameClient::Handle_ConnectButtonClicked, this));
		m_RootWindow->getChild("PlayerNameEdit")->subscribeEvent(Window::EventKeyDown, Event::Subscriber(&GameClient::Handle_ConnectKeyDown, this));
		m_RootWindow->getChild("ServerNameEdit")->subscribeEvent(Window::EventKeyDown, Event::Subscriber(&GameClient::Handle_ConnectKeyDown, this));

		m_RootWindow->getChild("CanvasScaleScrollbar")->subscribeEvent(Scrollbar::EventScrollPositionChanged, Event::Subscriber(&GameClient::Handle_CanvasScaleScrolled, this));

		m_RootWindow->getChild("ChatEdit")->subscribeEvent(Window::EventKeyDown, Event::Subscriber(&GameClient::Handle_ChatEditKeyDown, this));
		m_RootWindow->getChild("GuessEdit")->subscribeEvent(Window::EventKeyDown, Event::Subscriber(&GameClient::Handle_GuessEditKeyDown, this));

		m_RootWindow->getChild("ClearButton")->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GameClient::Handle_ClearButtonClicked, this));
	}
}

void GameClient::InitCanvas()
{
	//AssertReturnIf(m_CanvasBuffer);

	using namespace CEGUI;

	Window* canvasWindow = m_RootWindow->getChild("CanvasWindow");

	Sizef canvasSize = canvasWindow->getPixelSize() * m_CanvasScale;
	m_CanvasSize = Size<int>((int)canvasSize.d_width, (int)canvasSize.d_height);

	OpenGL3Renderer* renderer = static_cast<OpenGL3Renderer*>(System::getSingleton().getRenderer());
	m_CanvasTexture = &renderer->createTexture("CanvasTexture", canvasSize);

	BasicImage* canvasImage = static_cast<BasicImage*>(&ImageManager::getSingleton().create("BasicImage", "DrawEetLook/CanvasImage"));

	m_CanvasArea = Rectf(0.0f, 0.0f, (float)m_CanvasSize.d_width, (float)m_CanvasSize.d_height);

	canvasImage->setArea(m_CanvasArea);
	canvasImage->setAutoScaled(ASM_Disabled);

	canvasImage->setTexture(m_CanvasTexture);

	canvasWindow->setProperty<Image*>("CanvasImage", canvasImage);

	m_CanvasGLBuffer.Init(m_CanvasSize.d_width, m_CanvasSize.d_height, static_cast<OpenGLTexture*>(m_CanvasTexture)->getOpenGLTexture());
	//m_CanvasBuffer = static_cast<uint32_t*>(calloc(m_CanvasSize.d_width * m_CanvasSize.d_height, sizeof(*m_CanvasBuffer)));

	canvasWindow->subscribeEvent(Window::EventMouseMove, Event::Subscriber(&GameClient::Handle_CanvasMouseMove, this));
	canvasWindow->subscribeEvent(Window::EventMouseButtonDown, Event::Subscriber(&GameClient::Handle_CanvasMouseDown, this));
	canvasWindow->subscribeEvent(Window::EventMouseButtonUp, Event::Subscriber(&GameClient::Handle_CanvasMouseUp, this));
	canvasWindow->subscribeEvent(Window::EventMouseLeavesArea, Event::Subscriber(&GameClient::Handle_CanvasMouseLeave, this));
	canvasWindow->subscribeEvent(Window::EventMouseEntersArea, Event::Subscriber(&GameClient::Handle_CanvasMouseEnter, this));
	canvasWindow->subscribeEvent(Window::EventMouseWheel, Event::Subscriber(&GameClient::Handle_CanvasMouseWheel, this));

}

void GameClient::ShowChatMessage(CEGUI::String message)
{
	using namespace CEGUI;

	ListboxTextItem* chatBoxItem = new ListboxTextItem(message);
	chatBoxItem->setTextParsingEnabled(false);
	Listbox* chatBox = static_cast<Listbox*>(m_RootWindow->getChild("ChatBox"));
	chatBox->addItem(chatBoxItem);

	if (chatBox->getItemCount() > CHAT_HISTORY)
	{
		chatBox->removeItem(chatBox->getListboxItemFromIndex(0));
	}

	chatBox->ensureItemIsVisible(chatBoxItem);
}

#if WEIRD_THING || WEIRD_THING_CHANGE_BRUSH
void changeColour(CEGUI::Colour& colour, int colourIndex, signed char& changing)
{
	const float oneBitChange = 1.0f / 256.0f;

	switch (colourIndex)
	{
		case 0:
		{
			if ((changing > 0 && colour.getRed() == 1.0f) ||
				(changing < 0 && colour.getRed() == 0.0f))
			{
				changing = -changing;
			}

			colour.setRed(colour.getRed() + changing * oneBitChange);
		} break;
		case 1:
		{
			if ((changing > 0 && colour.getGreen() == 1.0f) ||
				(changing < 0 && colour.getGreen() == 0.0f))
			{
				changing = -changing;
			}

			colour.setGreen(colour.getGreen() + changing * oneBitChange);
		} break;
		case 2:
		{
			if ((changing > 0 && colour.getBlue() == 1.0f) ||
				(changing < 0 && colour.getBlue() == 0.0f))
			{
				changing = -changing;
			}

			colour.setBlue(colour.getBlue() + changing * oneBitChange);
		} break;
#if WEIRD_THING_USE_ALPHA
		case 3:
		{
			if ((changing > 0 && colour.getAlpha() == 1.0f) ||
				(changing < 0 && colour.getAlpha() == 0.0f))
			{
				changing = -changing;
			}

			colour.setAlpha(colour.getAlpha() + changing * oneBitChange);
		} break;
#endif
	}
}
#endif

void GameClient::Update()
{
	using namespace CEGUI;

	GUIContext& context = System::getSingleton().getDefaultGUIContext();

	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			{
				m_Running = false;
				return;
			} break;
		case SDL_MOUSEMOTION:
			{
				context.injectMousePosition((float)event.motion.x, (float)event.motion.y);
			} break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			{
				int buttonCode = -1;
				switch (event.button.button)
				{
					case SDL_BUTTON_LEFT:
					{
						buttonCode = LeftButton;
					} break;
					case SDL_BUTTON_RIGHT:
					{
						buttonCode = RightButton;
					} break;
					case SDL_BUTTON_MIDDLE:
					{
						buttonCode = MiddleButton;
					} break;
				}

				if (buttonCode >= 0)
				{
					if (event.button.type == SDL_MOUSEBUTTONDOWN)
					{
						context.injectMouseButtonDown((MouseButton)buttonCode);
					}
					else
					{
						context.injectMouseButtonUp((MouseButton)buttonCode);
					}
				}
			} break;
		case SDL_MOUSEWHEEL:
			{
				context.injectMouseWheelChange((float)event.wheel.y);
			} break;
		case SDL_KEYDOWN:
			{
				context.injectKeyDown(m_Keymap[event.key.keysym.scancode]);
			} break;
		case SDL_KEYUP:
			{
				context.injectKeyUp(m_Keymap[event.key.keysym.scancode]);
			} break;
		case SDL_TEXTINPUT:
			{
				String unicodeString = (uint8_t*)event.text.text;

				context.injectChar(unicodeString[0]);
			} break;
		}
	}

	if (m_PassButter.Type == PBPass)
	{
		unsigned chunksToDraw = (m_Butter.size() / 10) + 1;
		while (!m_Butter.empty() && chunksToDraw)
		{
			ButterChunk& chunk = m_Butter.front();
			switch (chunk.Type)
			{
			case BCPosition:
				{
					ButterPosition position = chunk.Positions.front();
					CanvasDrawChunk(Vector2f(position.X, position.Y));
					m_IsDrawing = true;
					--chunksToDraw;

					chunk.Positions.erase(chunk.Positions.begin());
					if (chunk.Positions.empty())
					{
						m_Butter.erase(m_Butter.begin());
					}
				} break;
			case BCColor:
				{
					m_CanvasBrushColor = chunk.Color;
					m_Butter.erase(m_Butter.begin());
				} break;
			case BCWidth:
				{
					m_CanvasBrushSize = (float)chunk.Width;
					m_Butter.erase(m_Butter.begin());
				} break;
			case BCEraser:
				{
					m_CanvasEraser = true;
					m_Butter.erase(m_Butter.begin());
				} break;
			case BCClear:
				{
					m_CanvasGLBuffer.Clear();
					//memset((void*)m_CanvasBuffer, 0, m_CanvasSize.d_width * m_CanvasSize.d_height * sizeof(*m_CanvasBuffer));
					//m_CanvasTexture->blitFromMemory(m_CanvasBuffer, m_CanvasArea);
					m_Butter.erase(m_Butter.begin());
				} break;
			case BCStop:
				{
					m_IsDrawing = false;
					m_CanvasEraser = false;
					m_Butter.erase(m_Butter.begin());
				} break;
			}
		}
	}
	else if (m_PassButter.Type == PBMake)
	{
		Window* canvasWindow = m_RootWindow->getChild("CanvasWindow");

		if (context.getWindowContainingMouse() == canvasWindow && m_ShouldRedrawBrushPreview)
		{
			DrawBrushPreview();
		}
	}

	double t = 0.001 * SDL_GetTicks();
	float deltaT = (float)(t - m_LastTimePulse);
	context.injectTimePulse(deltaT);
	System::getSingleton().injectTimePulse(deltaT);
	m_LastTimePulse = t;

	if (m_PassButter.Type != PBWait)
	{
		int timeRemaining = (int)m_PassButter.TimeRemaining - (int)((SDL_GetTicks() / 1000) - m_TurnStart);

		if (0 <= timeRemaining && timeRemaining < 1000)
		{
			char timeText[5];
			sprintf(timeText, "%d", timeRemaining);

			m_RootWindow->getChild("TimeText")->setText(timeText);
		}
	}

#if WEIRD_THING
	{
		if (rand() % 60 == 0)
		{
			int index = rand() % 4;
#if WEIRD_THING_USE_ALPHA
			int color = rand() % 4;
#else
			int color = rand() % 3;
#endif

			if (m_CanvasColorRectChanging[index][color] == 0)
			{
				m_CanvasColorRectChanging[index][color] = (rand() % 2) * 2 - 1;
			}
			else
			{
				m_CanvasColorRectChanging[index][color] = 0;
			}
		}

		Colour& tl = m_CanvasColorRect.d_top_left;
		Colour& tr = m_CanvasColorRect.d_top_right;
		Colour& bl = m_CanvasColorRect.d_bottom_left;
		Colour& br = m_CanvasColorRect.d_bottom_right;

		for (int i = 0; i < 4; ++i)
		{
			for (int c = 0; c < 4; ++c)
			{
				signed char& changing = m_CanvasColorRectChanging[i][c];
				if (changing != 0)
				{
					switch (i)
					{
						case 0:
						{
							changeColour(tl, c, changing);
						} break;
						case 1:
						{
							changeColour(tr, c, changing);
						} break;
						case 2:
						{
							changeColour(bl, c, changing);
						} break;
						case 3:
						{
							changeColour(br, c, changing);
						} break;
					}
				}
			}
		}

		//char buffer[50];
		//sprintf(buffer, "tl:%08X tr:%08X bl:%08X br:%08X", tl.getARGB(), tr.getARGB(), bl.getARGB(), br.getARGB());

		m_RootWindow->getChild("CanvasWindow")->setProperty<ColourRect>("ImageColours", m_CanvasColorRect);
	}
#endif

	System::getSingleton().renderAllGUIContexts();

	SDL_GL_SwapWindow(m_SDLWindow);

	/*if (!m_Vsync)
	{
		SDL_Delay(FAKE_VSYNC_DELAY);
	}*/

	// Network stuff
	if (m_ClientSocket)
	{
		if (SDLNet_CheckSockets(m_SocketSet, 0))
		{
			while (SDLNet_SocketReady(m_ClientSocket))
			{
				int receivedBytes = SDLNet_TCP_Recv(m_ClientSocket, (void*)m_PacketBuffer, sizeof(m_PacketBuffer));

				if (receivedBytes <= 0)
				{
					m_Running = false;
				}
				else
				{
					AssertBreakIf(m_InBufferOffset + receivedBytes > sizeof(m_InBuffer));

					memcpy((void*)(m_InBuffer + m_InBufferOffset), (void*)m_PacketBuffer, receivedBytes);
					m_InBufferOffset += receivedBytes;

					while (m_InBufferOffset > 0 && m_InBufferOffset >= m_InMessageSize)
					{
						if (m_InMessageSize > 0)
						{
							unsigned messageSize = ProcessBuffer((m_InBuffer + 2), sizeof(m_InBuffer) - 2) + 2;
							ASSERT(messageSize == m_InMessageSize);

							m_InMessageSize = 0;
							m_InBufferOffset -= messageSize;
							memmove((void*)m_InBuffer, (void*)(m_InBuffer + messageSize), m_InBufferOffset);
						}

						if (m_InBufferOffset >= 2)
						{
							unsigned offsetStart = 0;
							m_InMessageSize = BufferReadUInt16(m_InBuffer, sizeof(m_InBuffer), offsetStart) + 2;
						}
					}
				}
			}
		}

		if (m_PassButter.Type == PBMake)
		{
			if (!m_Butter.empty())
			{
				unsigned now = SDL_GetTicks();
				if ((m_Butter.size() >= BUTTER_CHUNKS) || (now - m_ButterTime >= BUTTER_INTERVAL))
				{
					m_ButterTime = now;
					char* buffer = m_OutBuffer + m_OutBufferOffset;
					unsigned bufferSize = sizeof(m_OutBuffer) - m_OutBufferOffset;

					m_OutBufferOffset += GenerateMessageButter(buffer, bufferSize, m_Butter, m_MyIndex);

					m_Butter.clear();
				}
			}
		}

		{
			unsigned bytesToSend = PACKET_SIZE;
			if (m_OutBufferOffset < bytesToSend)
			{
				bytesToSend = m_OutBufferOffset;
			}

			if (bytesToSend > 0)
			{
				int bytesSent = SDLNet_TCP_Send(m_ClientSocket, (void*)m_OutBuffer, bytesToSend);

				if (bytesSent < (int)bytesToSend)
				{
					// TODO: Handle send error
					ASSERT(false);
				}

				m_OutBufferOffset -= bytesSent;
				memmove((void*)m_OutBuffer, (void*)(m_OutBuffer + bytesSent), m_OutBufferOffset);
			}
		}
	}
}

bool GameClient::Handle_CanvasMouseMove(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);
	
	Vector2f mouseDist = eventArgs.position - m_OldMousePos;
	m_AccumulatedMovement += mouseDist.d_x * mouseDist.d_x + mouseDist.d_y * mouseDist.d_y;
	m_OldMousePos = eventArgs.position;

	float mtr = 0.1f;  // Movement Threshold Relative
	float mta = 10.0f; // Movement Threshold Absolute

	if (m_IsDrawing && (m_AccumulatedMovement >= mtr * mtr * m_CanvasBrushSize * m_CanvasBrushSize || m_AccumulatedMovement >= mta * mta))
	{
		m_AccumulatedMovement = 0.0f;
		const Vector2f& windowPos = eventArgs.window->getPixelPosition();
		const Sizef& windowSize = eventArgs.window->getPixelSize();
		CanvasDrawChunk((eventArgs.position - windowPos) / Vector2f(windowSize.d_width, windowSize.d_height));
	}

	return true;
}

bool GameClient::Handle_CanvasMouseDown(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);

	if (eventArgs.button == LeftButton || eventArgs.button == RightButton)
	{
		if (!m_CanvasEraser && eventArgs.button == RightButton)
		{
			ButterChunk chunk;
			chunk.Type = BCEraser;
			m_Butter.push_back(chunk);
			m_CanvasEraser = true;
		}

		m_AccumulatedMovement = 0.0f;
		const Vector2f& windowPos = eventArgs.window->getPixelPosition();
		const Sizef& windowSize = eventArgs.window->getPixelSize();
		CanvasDrawChunk((eventArgs.position - windowPos) / Vector2f(windowSize.d_width, windowSize.d_height));
		m_IsDrawing = true;
	}

	return true;
}

bool GameClient::Handle_CanvasMouseUp(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);

	if (eventArgs.button == LeftButton || eventArgs.button == RightButton)
	{
		if (m_IsDrawing)
		{
			ButterChunk chunk;
			chunk.Type = BCStop;
			m_Butter.push_back(chunk);
			m_IsDrawing = false;
			m_CanvasEraser = false;
		}
	}

	return true;
}

bool GameClient::Handle_CanvasMouseLeave(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);

	if (m_IsDrawing)
	{
		ButterChunk chunk;
		chunk.Type = BCStop;
		m_Butter.push_back(chunk);
		m_IsDrawing = false;
		m_CanvasEraser = false;
	}

	return true;
}

bool GameClient::Handle_CanvasMouseEnter(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);

	if (eventArgs.sysKeys & (LeftMouse | RightMouse))
	{
		if (!m_CanvasEraser && eventArgs.sysKeys & RightMouse)
		{
			ButterChunk chunk;
			chunk.Type = BCEraser;
			m_Butter.push_back(chunk);
			m_CanvasEraser = true;
		}

		m_OldMousePos = eventArgs.position;
		m_AccumulatedMovement = 0.0f;
		const Vector2f& windowPos = eventArgs.window->getPixelPosition();
		const Sizef& windowSize = eventArgs.window->getPixelSize();
		CanvasDrawChunk((eventArgs.position - windowPos) / Vector2f(windowSize.d_width, windowSize.d_height));
		m_IsDrawing = true;
	}

	return true;
}

bool GameClient::Handle_CanvasMouseWheel(const CEGUI::EventArgs& e)
{
	ReturnUnless(m_PassButter.Type == PBMake, true);

	using namespace CEGUI;

	const MouseEventArgs& eventArgs = static_cast<const MouseEventArgs&>(e);

	float change = eventArgs.wheelChange;

	if (m_CanvasBrushSize > MAX_BRUSH_SIZE / 2.0f)
	{
		change *= 2.0f;
	}

	m_CanvasBrushSize += change;

	m_CanvasBrushSize = std::max(MIN_BRUSH_SIZE, std::min(m_CanvasBrushSize, MAX_BRUSH_SIZE));
	m_CanvasBrushSize = floorf(m_CanvasBrushSize + 0.5f); // Round it just in case

	m_ShouldRedrawBrushPreview = true;

	if (m_Butter.empty() || m_Butter.back().Type != BCWidth)
	{
		ButterChunk chunk;
		chunk.Type = BCWidth;
		m_Butter.push_back(chunk);
	}
	m_Butter.back().Width = (int)m_CanvasBrushSize;

	return true;
}

void GameClient::Connect()
{
	using namespace CEGUI;

	if (InitSDLNet())
	{
		m_RootWindow->getChild("PlayerNameEdit")->disable();
		m_RootWindow->getChild("ServerNameEdit")->disable();
		m_RootWindow->getChild("ConnectButton")->disable();

		InitCanvas();
	}
	else
	{
		m_ConnectingToServer = false;
	}
}

bool GameClient::Handle_ConnectButtonClicked(const CEGUI::EventArgs& e)
{
	Connect();

	return true;
}

bool GameClient::Handle_ConnectKeyDown(const CEGUI::EventArgs& e)
{
	using namespace CEGUI;

	const KeyEventArgs& eventArgs = static_cast<const KeyEventArgs&>(e);

	if (eventArgs.scancode == Key::Return || eventArgs.scancode == Key::NumpadEnter)
	{
		Connect();
		return true;
	}

	return false;
}

bool GameClient::Handle_CanvasScaleScrolled(const CEGUI::EventArgs& e)
{
	using namespace CEGUI;

	const WindowEventArgs& eventArgs = static_cast<const WindowEventArgs&>(e);

	// Between 0 and 90, with a step of 10
	float scrollPos = static_cast<Scrollbar*>(eventArgs.window)->getScrollPosition();
	scrollPos = std::max(0.0f, std::min(floorf((scrollPos / 10.0f) + 0.5f) * 10.0f, 90.0f));

	m_CanvasScale = (scrollPos + 10.0f) / 100.0f;

	char scaleText[10];
	sprintf(scaleText, "%3.1f", m_CanvasScale);

	m_RootWindow->getChild("CanvasScaleText")->setText(scaleText);
	
	return true;
}

bool GameClient::Handle_ChatEditKeyDown(const CEGUI::EventArgs& e)
{
	using namespace CEGUI;

	const KeyEventArgs& eventArgs = static_cast<const KeyEventArgs&>(e);

	if (eventArgs.scancode == Key::Return || eventArgs.scancode == Key::NumpadEnter)
	{
		String message = m_RootWindow->getChild("ChatEdit")->getText();
		m_RootWindow->getChild("ChatEdit")->setText("");

		if (!message.empty())
		{
			char* buffer = m_OutBuffer + m_OutBufferOffset;
			unsigned bufferSize = sizeof(m_OutBuffer) - m_OutBufferOffset;

			if (message == "/start")
			{
				m_OutBufferOffset += GenerateMessageStart(buffer, bufferSize);
			}
			else
			{
				std::string utf8NameText = "[" + m_MyName + "]: ";
				String nameText = (uint8_t*)utf8NameText.c_str();
				String messageToShow = nameText + message;
				ShowChatMessage(messageToShow);

				m_OutBufferOffset += GenerateMessageChat(buffer, bufferSize, std::string(message.c_str()));
			}
		}

		return true;
	}

	return false;
}

bool GameClient::Handle_GuessEditKeyDown(const CEGUI::EventArgs& e)
{
	using namespace CEGUI;

	const KeyEventArgs& eventArgs = static_cast<const KeyEventArgs&>(e);

	if (eventArgs.scancode == Key::Return || eventArgs.scancode == Key::NumpadEnter)
	{
		std::string word = m_RootWindow->getChild("GuessEdit")->getText().c_str();
		m_RootWindow->getChild("GuessEdit")->setText("");

		if (!word.empty())
		{
			m_RootWindow->getChild("GuessEdit")->disable();

			Guess guess;
			guess.Type = GAttempt;
			guess.Word = word;
			char* buffer = m_OutBuffer + m_OutBufferOffset;
			unsigned bufferSize = sizeof(m_OutBuffer) - m_OutBufferOffset;

			m_OutBufferOffset += GenerateMessageGuess(buffer, bufferSize, guess);
		}

		return true;
	}

	return false;
}

bool GameClient::Handle_ColorClicked(const CEGUI::EventArgs& e)
{
	AssertReturnIf(m_PassButter.Type != PBMake, true);
	using namespace CEGUI;

	const WindowEventArgs& eventArgs = static_cast<const WindowEventArgs&>(e);

	m_CanvasBrushColor = eventArgs.window->getProperty<ColourRect>("ImageColours").d_top_left;

	m_ShouldRedrawBrushPreview = true;

	uint32_t color = m_CanvasBrushColor.getARGB() & 0x00FFFFFF;
	if (m_Butter.empty() || m_Butter.back().Type != BCColor)
	{
		ButterChunk chunk;
		chunk.Type = BCColor;
		m_Butter.push_back(chunk);
	}
	m_Butter.back().Color = color;

	m_RootWindow->getChild("ColorBox/Selection")->setPosition(eventArgs.window->getPosition() - UVector2(UDim(0.0f, 2.0f), UDim(0.0f, 2.0f)));

	return true;
}

bool GameClient::Handle_ClearButtonClicked(const CEGUI::EventArgs& e)
{
	if (m_Butter.empty() || m_Butter.back().Type != BCClear)
	{
		m_CanvasGLBuffer.Clear();
		//memset((void*)m_CanvasBuffer, 0, m_CanvasSize.d_width * m_CanvasSize.d_height * sizeof(*m_CanvasBuffer));
		//m_CanvasTexture->blitFromMemory(m_CanvasBuffer, m_CanvasArea);

		ButterChunk chunk;
		chunk.Type = BCClear;
		m_Butter.push_back(chunk);
	}

	return true;
}

void GameClient::UpdatePlayersData()
{
	using namespace CEGUI;

	Listbox* playerBox = static_cast<Listbox*>(m_RootWindow->getChild("PlayerBox"));
	
	unsigned i = 0;
	auto orderIt = m_PlayerOrder.begin();
	auto playerIndexInfo = m_PlayersData.begin();
	for (; orderIt != m_PlayerOrder.end() || playerIndexInfo != m_PlayersData.end();)
	{
		bool toRemove = (playerIndexInfo == m_PlayersData.end());
		bool toInsert = (orderIt == m_PlayerOrder.end());
		if (!(toRemove || toInsert))
		{
			bool matching = (*orderIt == playerIndexInfo->first);
			if (!matching)
			{
				toRemove = (m_PlayersData.find(*orderIt) == m_PlayersData.end());
				toInsert = !toRemove;
			}
		}

		ListboxTextItem* playerEntry = (i < m_PlayerOrder.size()) ? static_cast<ListboxTextItem*>(playerBox->getListboxItemFromIndex(i)) : 0;
		if (toRemove)
		{
			playerBox->removeItem(playerEntry);
			orderIt = m_PlayerOrder.erase(orderIt);
			continue;
		}
		else if (toInsert)
		{
			ListboxTextItem* newPlayerEntry = new ListboxTextItem("");
			newPlayerEntry->setTextParsingEnabled(false);
			newPlayerEntry->setSelectionColours(0xFF000080);
			newPlayerEntry->setSelectionBrushImage("TaharezLook/GenericBrush");

			if (playerEntry)
			{
				playerBox->insertItem(newPlayerEntry, playerEntry);
			}
			else
			{
				playerBox->addItem(newPlayerEntry);
			}

			playerEntry = newPlayerEntry;
			orderIt = m_PlayerOrder.insert(orderIt, playerIndexInfo->first);
		}

		char pointsText[4];
		if (playerIndexInfo->second.Points < 1000)
		{
			sprintf(pointsText, "%d", playerIndexInfo->second.Points);
		}

		bool showPlus = m_GuessedThisTurn.find(playerIndexInfo->first) != m_GuessedThisTurn.end() && m_GuessedThisTurn.at(playerIndexInfo->first);
		playerEntry->setText(playerIndexInfo->second.Name + ": " + pointsText + (showPlus ? " +" : ""));

		if (m_PassButter.Type != PBWait)
		{
			playerEntry->setSelected(playerIndexInfo->first == m_PassButter.DrawingPlayer);
		}

		++i;
		++orderIt;
		++playerIndexInfo;
	}

	playerBox->invalidate();
}

void GameClient::CanvasDrawChunk(CEGUI::Vector2f relativeMousePos)
{
	using namespace CEGUI;

	if (m_PassButter.Type == PBMake)
	{
#if WEIRD_THING_CHANGE_BRUSH
		if (!m_CanvasEraser)
		{
			int changeColor = rand() % 4;

			if (m_CanvasBrushColorChanging[changeColor] == 0)
			{
				m_CanvasBrushColorChanging[changeColor] = ((rand() % 2) * 2 - 1) * 4;
			}
			else
			{
				m_CanvasBrushColorChanging[changeColor] = 0;
			}

			for (int c = 0; c < 4; ++c)
			{
				signed char& changing = m_CanvasBrushColorChanging[c];
				if (changing != 0)
				{
					changeColour(m_CanvasBrushColor, c, changing);
				}
			}

			m_ShouldRedrawBrushPreview = true;

			uint32_t color = m_CanvasBrushColor.getARGB() & 0x00FFFFFF;
			if (m_Butter.empty() || m_Butter.back().Type != BCColor)
			{
				ButterChunk chunk;
				chunk.Type = BCColor;
				m_Butter.push_back(chunk);
			}
			m_Butter.back().Color = color;
		}
#endif
		// Make sure the last butter chunk is positions
		if (m_Butter.empty() || m_Butter.back().Type != BCPosition)
		{
			ButterChunk chunk;
			chunk.Type = BCPosition;
			m_Butter.push_back(chunk);
		}

		ButterPosition butterPosition;
		butterPosition.X = relativeMousePos.d_x;
		butterPosition.Y = relativeMousePos.d_y;
		m_Butter.back().Positions.push_back(butterPosition);
	}

	m_CanvasGLBuffer.SetErasing(m_CanvasEraser);
	if (!m_IsDrawing)
	{
		m_OldBrushPos = relativeMousePos;
	}
	m_CapsuleShader.DrawChunk(m_CanvasGLBuffer, m_OldBrushPos, relativeMousePos, m_OldBrushColor, m_CanvasBrushColor, m_CanvasBrushSize * m_CanvasScale);

	m_OldBrushPos = relativeMousePos;
	m_OldBrushColor = m_CanvasBrushColor;

	/*
	// Apply the scale before thinking about coordinates
	Vector2f newPos = Vector2f((relativeMousePos.d_x * m_CanvasSize.d_width), (relativeMousePos.d_y * m_CanvasSize.d_height));

	float brushWidthf = m_CanvasBrushSize * m_CanvasScale;
	int brushWidth = (int)ceilf(brushWidthf);
	float solidDist = std::min(m_CanvasBrushHardness * brushWidthf, brushWidthf - MIN_FEATHER);

	uint32_t brushColor = m_CanvasBrushColor.getARGB() & 0x00FFFFFF;
	brushColor = ((brushColor & 0xFF0000) >> 16) + (brushColor & 0x00FF00) + ((brushColor & 0x0000FF) << 16);

	int minX, minY, maxX, maxY;
	if (m_IsDrawing)
	{
		minX = std::max(0, std::min((int)m_OldBrushPos.d_x, (int)newPos.d_x) - brushWidth);
		minY = std::max(0, std::min((int)m_OldBrushPos.d_y, (int)newPos.d_y) - brushWidth);
		maxX = std::min(std::max((int)m_OldBrushPos.d_x, (int)newPos.d_x) + brushWidth, m_CanvasSize.d_width - 1);
		maxY = std::min(std::max((int)m_OldBrushPos.d_y, (int)newPos.d_y) + brushWidth, m_CanvasSize.d_height - 1);
	}
	else
	{
		minX = std::max(0, (int)newPos.d_x - brushWidth);
		minY = std::max(0, (int)newPos.d_y - brushWidth);
		maxX = std::min((int)newPos.d_x + brushWidth, m_CanvasSize.d_width - 1);
		maxY = std::min((int)newPos.d_y + brushWidth, m_CanvasSize.d_height - 1);
		m_OldBrushPos = newPos;
	}

	for (int y = minY; y <= maxY; ++y)
	{
		for (int x = minX; x <= maxX; ++x)
		{
			Vector2f p((float)x, (float)y);
			Vector2f ba = m_OldBrushPos - newPos;
			Vector2f pa = p - newPos;
			float lineLengthSqr = ba.d_x * ba.d_x + ba.d_y * ba.d_y;

			Vector2f dotVec = pa * ba;

			float t = std::max(0.0f, std::min((dotVec.d_x + dotVec.d_y) / lineLengthSqr, 1.0f));
			// Weird attempt to not have previous segment doubled
			if (m_IsDrawing && t >= 1.0f)
			{
				continue;
			}
			Vector2f dist = newPos + ba * t - p;

			float distsqr = dist.d_y * dist.d_y + dist.d_x * dist.d_x;

			if (distsqr < brushWidthf * brushWidthf)
			{
				uint32_t alpha = m_CanvasEraser ? 0xFF : 0;

				uint32_t color;
				if (!m_IsDrawing)
				{
					color = brushColor;
				}
				else
				{
					color = (m_OldBrushColor * t + m_CanvasBrushColor * (1.0f - t)).getARGB() & 0x00FFFFFF;
					color = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
				}
				// Do color blending and max alpha
				float newAlpha = (brushWidthf - sqrt(distsqr)) / (brushWidthf - solidDist);

				/* Weird attempt to make the capsule smoother
				if (m_IsDrawing && (1.0f - t) * sqrt(lineLengthSqr) < brushWidthf)
				{
					Vector2f distToOld = m_OldBrushPos - p;
					float distToOldSqr = distToOld.d_x * distToOld.d_x + distToOld.d_y * distToOld.d_y;

					float newAlphaToOld = sqrt(distToOldSqr) / brushWidthf;
					newAlpha = std::min(newAlpha, newAlphaToOld);
				}/

				if (newAlpha >= 1.0f)
				{
					alpha = m_CanvasEraser ? 0 : 0xFF;
				}
				else
				{
					if (m_CanvasEraser)
					{
						CEGUI::Colour oldPixel(m_CanvasBuffer[y * m_CanvasSize.d_width + x]);
						color = oldPixel.getARGB() & 0x00FFFFFF;
						alpha = (uint32_t)((1.0f - newAlpha) * oldPixel.getAlpha() * 0xFF);
					}
					else
					{
						CEGUI::Colour newPixel(color + (((uint32_t)(newAlpha * 0xFF)) << 24));

						if (newPixel.getAlpha() > 0.0f)
						{
							CEGUI::Colour oldPixel(m_CanvasBuffer[y * m_CanvasSize.d_width + x]);
							float oldAlpha = oldPixel.getAlpha() * (1 - newPixel.getAlpha());
							float oldPart = oldAlpha / (oldAlpha + newPixel.getAlpha());
							float newPart = 1.0f - oldPart;

							color = ((uint32_t)((oldPixel.getRed()	* oldPart + newPixel.getRed()	* newPart) * 0xFF) << 16)
								+ ((uint32_t)((oldPixel.getGreen()	* oldPart + newPixel.getGreen()	* newPart) * 0xFF) << 8)
								+ (uint32_t)((oldPixel.getBlue()	* oldPart + newPixel.getBlue()	* newPart) * 0xFF);

							alpha = (uint32_t)((oldAlpha + newPixel.getAlpha()) * 0xFF);
						}
					}
				}

				bool draw = m_CanvasEraser ? (alpha < 0xFF) : (alpha > 0);
				if (draw)
				{
					m_CanvasBuffer[y * m_CanvasSize.d_width + x] = color + (alpha << 24);
				}
			}
		}
	}

	m_CanvasTexture->blitFromMemory(m_CanvasBuffer, m_CanvasArea);

	m_OldBrushPos = newPos;
	m_OldBrushColor = m_CanvasBrushColor;
	*/
}

void GameClient::DrawBrushPreview()
{
	using namespace CEGUI;

	m_BrushPreviewGLBuffer.Clear(CEGUI::Colour(1.0f - m_CanvasBrushColor.getRed(), 1.0f - m_CanvasBrushColor.getGreen(), 1.0f - m_CanvasBrushColor.getBlue()));
	Vector2f pos(0.5f, 0.5f);
	m_CapsuleShader.DrawChunk(m_BrushPreviewGLBuffer, pos, pos, m_CanvasBrushColor, m_CanvasBrushColor, m_CanvasBrushSize);

	/*
	memset((void*)m_BrushPreviewBuffer, 0, BRUSH_PREVIEW_SIZE * BRUSH_PREVIEW_SIZE * sizeof(*m_BrushPreviewBuffer));

	int halfBrushPreviewSize = BRUSH_PREVIEW_SIZE / 2;
	Vector2f pos((float)halfBrushPreviewSize, (float)halfBrushPreviewSize);
	
	float brushWidthf = m_CanvasBrushSize;
	int brushWidth = (int)ceilf(brushWidthf);
	float solidDist = std::min(m_CanvasBrushHardness * brushWidthf, brushWidthf - MIN_FEATHER);

	uint32_t brushColor = m_CanvasBrushColor.getARGB() & 0x00FFFFFF;
	brushColor = ((brushColor & 0xFF0000) >> 16) + (brushColor & 0x00FF00) + ((brushColor & 0x0000FF) << 16);

	int minX = std::max(0, (int)pos.d_x - brushWidth);
	int minY = std::max(0, (int)pos.d_y - brushWidth);
	int maxX = std::min((int)pos.d_x + brushWidth, (int)BRUSH_PREVIEW_SIZE - 1);
	int maxY = std::min((int)pos.d_y + brushWidth, (int)BRUSH_PREVIEW_SIZE - 1);

	for (int y = minY; y <= maxY; ++y)
	{
		for (int x = minX; x <= maxX; ++x)
		{
			Vector2f p((float)x, (float)y);

			Vector2f dist = pos - p;

			float distsqr = dist.d_y * dist.d_y + dist.d_x * dist.d_x;

			if (distsqr < brushWidthf * brushWidthf)
			{
				float newAlpha = (brushWidthf - sqrt(distsqr)) / (brushWidthf - solidDist);

				uint32_t alpha = std::min((uint32_t)(newAlpha * 0xFF), (uint32_t)0xFF);

				if (alpha > 0)
				{
					m_BrushPreviewBuffer[y * BRUSH_PREVIEW_SIZE + x] = brushColor + (alpha << 24);
				}
			}
		}
	}

	m_BrushPreviewTexture->blitFromMemory(m_BrushPreviewBuffer, m_BrushPreviewArea);
	*/
	System::getSingleton().getDefaultGUIContext().getMouseCursor().invalidate();
	m_ShouldRedrawBrushPreview = false;
}

int SDL_main(int argc, char* argv[])
{
	if (!m_GameClient.InitSDL())
	{
		exit(EXIT_FAILURE);
	}

	m_GameClient.InitCEGUI();

	while (m_GameClient.m_Running)
	{
		m_GameClient.Update();
	}

	return 0;
}