#pragma once

struct Framebuffer
{
	void Init(GLsizei width, GLsizei height, GLuint textureId);
	void Clear(CEGUI::Colour color = CEGUI::Colour(1.0f, 1.0f, 1.0f));

	void SetErasing(bool erasing);

	GLuint m_Framebuffer;
	GLsizei m_Width;
	GLsizei m_Height;
};

void Framebuffer::Init(GLsizei width, GLsizei height, GLuint textureId)
{
	m_Width = width;
	m_Height = height;

	GLuint oldFramebuffer;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&oldFramebuffer);

	glGenFramebuffers(1, &m_Framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	glBindFramebuffer(GL_FRAMEBUFFER, oldFramebuffer);
}

void Framebuffer::Clear(CEGUI::Colour color)
{
	GLuint oldFramebuffer;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&oldFramebuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);

	glClearColor(color.getRed(), color.getGreen(), color.getBlue(), 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, oldFramebuffer);
}

void Framebuffer::SetErasing(bool erasing)
{
	if (erasing)
	{
		glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}
}

struct CapsuleShader
{
	void Init(const char* vertexShaderFile, const char* geometryShaderFile, const char* fragmentShaderFile);
	void DrawChunk(Framebuffer& framebuffer, CEGUI::Vector2f oldPos, CEGUI::Vector2f newPos, CEGUI::Colour oldColor, CEGUI::Colour newColor, float brushSize);

	GLuint m_Program;

	GLint m_PositionAttribute;
	GLint m_ColorAttribute;

	GLint m_gsWidthUniform;
	GLint m_segmentUniform;
	GLint m_fsWidthUniform;

	GLuint m_VAO;
	GLuint m_VBO;
};

void ReadShaderFromFile(const char* fileName, char*& buffer)
{
	FILE* file = fopen(fileName, "rb");
	if (!file)
	{
		fprintf(stderr, "Failed to open file %s!", fileName);
		ASSERT(false);
	}
	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	rewind(file);

	buffer = (char*)malloc((size + 1) * sizeof(char));
	if (!buffer)
	{
		fprintf(stderr, "Could not allocate memory!");
		ASSERT(false);
	}

	size_t result = fread(buffer, 1, size, file);
	if (result != size)
	{
		fprintf(stderr, "WTF?");
		ASSERT(false);
	}
	(buffer)[size] = 0;

	fclose(file);
}

GLuint LoadAndCompileShader(const char* fileName, GLenum shaderType)
{
	char* buffer;
	ReadShaderFromFile(fileName, buffer);

	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, (const GLchar**)&buffer, NULL);
	free(buffer);

	glCompileShader(shader);

	GLint test;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &test);
	if (!test)
	{
		char compilationLog[512];
		glGetShaderInfoLog(shader, sizeof(compilationLog), NULL, compilationLog);
		fprintf(stderr, "Shader compilation failed!\nLog: %s\n", compilationLog);
		ASSERT(false);
	}
	return shader;
}

void CapsuleShader::Init(const char* vertexShaderFile, const char* geometryShaderFile, const char* fragmentShaderFile)
{
	GLuint vertexShader = LoadAndCompileShader(vertexShaderFile, GL_VERTEX_SHADER);
	GLuint geometryShader = LoadAndCompileShader(geometryShaderFile, GL_GEOMETRY_SHADER);
	GLuint fragmentShader = LoadAndCompileShader(fragmentShaderFile, GL_FRAGMENT_SHADER);

	m_Program = glCreateProgram();
	glAttachShader(m_Program, vertexShader);
	glAttachShader(m_Program, geometryShader);
	glAttachShader(m_Program, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(geometryShader);
	glDeleteShader(fragmentShader);

	glLinkProgram(m_Program);

	m_PositionAttribute = glGetAttribLocation(m_Program, "inPosition");
	m_ColorAttribute = glGetAttribLocation(m_Program, "inColor");

	m_gsWidthUniform = glGetUniformLocation(m_Program, "gsWidth");
	m_segmentUniform = glGetUniformLocation(m_Program, "segment");
	m_fsWidthUniform = glGetUniformLocation(m_Program, "fsWidth");

	GLuint oldProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, (GLint*)&oldProgram);

	GLuint oldVAO;
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, (GLint*)&oldVAO);

	GLuint oldVBO;
	glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&oldVBO);

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	glBufferData(GL_ARRAY_BUFFER, (2 * 2 + 2 * 4) * sizeof(GLfloat), nullptr, GL_DYNAMIC_DRAW);

	glUseProgram(m_Program);

	glVertexAttribPointer(m_PositionAttribute, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(m_PositionAttribute);

	glVertexAttribPointer(m_ColorAttribute, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*)((2 * 2) * sizeof(GLfloat)));
	glEnableVertexAttribArray(m_ColorAttribute);

	glUseProgram(oldProgram);

	glBindVertexArray(oldVAO);
	glBindBuffer(GL_ARRAY_BUFFER, oldVBO);
}

void CapsuleShader::DrawChunk(Framebuffer& framebuffer, CEGUI::Vector2f oldPos, CEGUI::Vector2f newPos, CEGUI::Colour oldColor, CEGUI::Colour newColor, float brushSize)
{
	GLuint oldFramebuffer;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&oldFramebuffer);

	GLint oldViewport[4];
	glGetIntegerv(GL_VIEWPORT, oldViewport);

	GLuint oldProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, (GLint*)&oldProgram);

	GLuint oldVAO;
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, (GLint*)&oldVAO);

	GLuint oldVBO;
	glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&oldVBO);

	GLboolean oldBlend = glIsEnabled(GL_BLEND);
	glEnable(GL_BLEND);

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.m_Framebuffer);
	glViewport(0, 0, framebuffer.m_Width, framebuffer.m_Height);

	glUseProgram(m_Program);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	GLfloat vertices[2 * 2] = {
		(oldPos.d_x * 2.0f) - 1.0f,	(oldPos.d_y * 2.0f) - 1.0f,
		(newPos.d_x * 2.0f) - 1.0f,	(newPos.d_y * 2.0f) - 1.0f
	};

	GLfloat colors[2 * 4] = {
		oldColor.getRed(),	oldColor.getGreen(),	oldColor.getBlue(),	1.0f,
		newColor.getRed(),	newColor.getGreen(),	newColor.getBlue(),	1.0f
	};

	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), (GLvoid*)vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), (GLvoid*)colors);

	GLfloat segment[2 * 2] = {
		//oldPos.d_x * framebuffer.m_Width,	oldPos.d_y * framebuffer.m_Height,
		//newPos.d_x * framebuffer.m_Width,	newPos.d_y * framebuffer.m_Height
		((vertices[0 * 2 + 0] + 1.0f) / 2.0f) * framebuffer.m_Width,	((vertices[0 * 2 + 1] + 1.0f) / 2.0f) * framebuffer.m_Height,
		((vertices[1 * 2 + 0] + 1.0f) / 2.0f) * framebuffer.m_Width,	((vertices[1 * 2 + 1] + 1.0f) / 2.0f) * framebuffer.m_Height
	};

	GLfloat gsWidth = std::max(brushSize / framebuffer.m_Width, brushSize / framebuffer.m_Height) * 2.0f;
	glUniform1f(m_gsWidthUniform, gsWidth);
	glUniform2fv(m_segmentUniform, 2, segment);
	glUniform1f(m_fsWidthUniform, brushSize);

	glDrawArrays(GL_LINES, 0, 2);

	if (oldBlend == GL_FALSE)
	{
		glDisable(GL_BLEND);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, oldFramebuffer);
	glViewport(oldViewport[0], oldViewport[1], (GLsizei)oldViewport[2], (GLsizei)oldViewport[3]);

	glUseProgram(oldProgram);

	glBindVertexArray(oldVAO);
	glBindBuffer(GL_ARRAY_BUFFER, oldVBO);
}
