#pragma once

#define BreakIf(x)				if ((x))	{ break;	}
#define BreakUnless(x)			if (!(x))	{ break;	}
#define ContinueIf(x)			if ((x))	{ continue;	}
#define ContinueUnless(x)		if (!(x))	{ continue;	}
#define ReturnIf(x, ...)		do { if ((x))	{ return __VA_ARGS__;	} } while(0)
#define ReturnUnless(x, ...)	do { if (!(x))	{ return __VA_ARGS__;	} } while(0)

#ifdef _DEBUG
#define ASSERT(x)				do { if (!(x))	{ __debugbreak(); } } while(0)
#else
#define ASSERT(x)
#endif

#define AssertBreakIf(x)			if ((x))	{ ASSERT(false); break;		}
#define AssertBreakUnless(x)		if (!(x))	{ ASSERT(false); break;		}
#define AssertContinueIf(x)			if ((x))	{ ASSERT(false); continue;	}
#define AssertContinueUnless(x)		if (!(x))	{ ASSERT(false); continue;	}
#define AssertReturnIf(x, ...)		do { if ((x))	{ ASSERT(false); return __VA_ARGS__;} } while(0)
#define AssertReturnUnless(x, ...)	do { if (!(x))	{ ASSERT(false); return __VA_ARGS__;} } while(0)
