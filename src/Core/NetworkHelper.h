#pragma once

#include <string>
#include <map>
#include <vector>

static const unsigned SERVER_PORT = 1337;
static const unsigned PACKET_SIZE = 512;
static const unsigned BUFFER_SIZE = 65536;

// First 2B of any message will contain the total size of the message (excluding these 2B)

enum MessageType : uint8_t
{
	MTUndefined = 0,	// This should never actually happen

	MTServerFull,		// Pretty self-explanatory
						// [MTServerFull]

	MTJoinSuccess,		// Successfully established connection to the server
						// [MTJoinSuccess][MyIndex = 1B]

	MTClientsData,		// Information about existing clients on the server
						// All of them are send to a newly connected player
						// Info about a new player is sent on its own to all the players
						// Info about a leaving player is sent with points and name length being at 0
						// Info about a successful guess (increase in points) sent about 2 players (drawing and guessing) without names but with total points
						// [MTClients][ClientCount = 1B]{[Index = 1B][Points = 1B][NameLength = 1B][Name = (NameLength)B]} x ClientCount

	MTClientInfo,		// Sent by client to "introduce" itself
						// [MTClientInfo][NameLength = 1B][Name = (NameLength)B]

	MTPassButter,		// This will tell us whether to make or pass butter
						// [MTPassButter]	[PBMake][TimeRemaining = 2B][TurnsLeft = 1B][WordLength = 1B][Word = (WordLength)B]
						//					[PBPass][TimeRemaining = 2B][TurnsLeft = 1B][DrawingPlayer = 1B]

	MTButter,			// The butter itself
						// [MTButter][SenderIndex = 1B][ChunkCount = 2B]{	[BCPosition][PositionCount = 2B]{[X = 4B][Y = 4B]} x PositionCount } x ChunkCount
						//													[BCColor]	[Color = 4B]
						//													[BCWidth]	[Width = 1B]
						//													[BCEraser]
						//													[BCClear]
						//													[BCStop]

	MTChat,				// Chat message
						// [MTChat][SenderIndex = 1B][MessageLength = 2B][Message = (MessageLength)B]

	MTGuess,			// Guess attempt or success
						// [MTGuess][GAttempt][SenderIndex = 1B][GuessLength = 1B][Guess = (GuessLength)B]
						//			[GSuccess][WordLength = 1B][Word = (WordLength)B]

	MTStart,			// Force start game
						// [MTStart]

	MTTurnEnd			// End of the turn: send the word to everyone
						// This should be received before MTPassButter, so the client should know who the drawing player was
						// [MTTurnEnd][WordLength = 1B][Word = (WordLength)B]
};

enum PassButterType : uint8_t
{
	PBWait = 0,
	PBMake,		// You make butter
	PBPass		// You pass butter
};

enum ButterChunkType : uint8_t
{
	BCPosition = 0,
	BCColor,
	BCWidth,
	BCEraser,
	BCClear,
	BCStop
};

enum GuessType : uint8_t
{
	GAttempt = 0,
	GSuccess
};

// Preprocessor magic, yay
#define BUFFER_READ_MACRO(name, type) \
	type BufferRead ## name(const char* buffer, unsigned bufferSize, unsigned& offset) {\
		AssertReturnIf(offset + sizeof(type) > bufferSize, 0);\
		type result = 0;\
		memcpy((void*)&result, (void*)(buffer + offset), sizeof(type));\
		offset += sizeof(type);\
		return result;\
	}

BUFFER_READ_MACRO(UInt8, uint8_t)
BUFFER_READ_MACRO(UInt16, uint16_t)
BUFFER_READ_MACRO(UInt32, uint32_t)
BUFFER_READ_MACRO(Float, float)

#undef BUFFER_READ_MACRO

#define BUFFER_WRITE_MACRO(name, type) \
	void BufferWrite ## name(char* buffer, unsigned bufferSize, unsigned& offset, type value) { \
		AssertReturnIf(offset + sizeof(type) > bufferSize); \
		memcpy((void*)(buffer + offset), (void*)&value, sizeof(type)); \
		offset += sizeof(type); \
	}

BUFFER_WRITE_MACRO(UInt8, uint8_t)
BUFFER_WRITE_MACRO(UInt16, uint16_t)
BUFFER_WRITE_MACRO(UInt32, uint32_t)
BUFFER_WRITE_MACRO(Float, float)

#undef BUFFER_WRITE_MACRO

void BufferReadBytes(const char* buffer, unsigned bufferSize, unsigned& offset, char* destination, unsigned bytes)
{
	ReturnIf(bytes == 0);
	AssertReturnIf(offset + bytes > bufferSize);
	memcpy((void*)destination, (void*)(buffer + offset), bytes);
	offset += bytes;
}

void BufferWriteBytes(char* buffer, unsigned bufferSize, unsigned& offset, const char* source, unsigned bytes)
{
	ReturnIf(bytes == 0);
	AssertReturnIf(offset + bytes > bufferSize);
	memcpy((void*)(buffer + offset), (void*)source, bytes);
	offset += bytes;
}

struct ClientInfo
{
	ClientInfo() : Points(0), Name() {}

	uint8_t		Points;
	std::string	Name;
};

typedef std::pair<uint8_t, ClientInfo> ClientIndexInfo;
typedef std::map<uint8_t, ClientInfo> ClientsData;

struct PassButter
{
	PassButter() : Type(PBWait), TimeRemaining(0), TurnsLeft(0), DrawingPlayer(0), Word() {}

	PassButterType	Type;
	uint16_t		TimeRemaining;
	uint8_t			TurnsLeft;
	uint8_t			DrawingPlayer;
	std::string		Word;
};

struct ButterPosition
{
	ButterPosition() : X(0.0f), Y(0.0f) {}

	float	X;
	float	Y;
};

struct ButterChunk
{
	ButterChunk() : Type(BCPosition), Positions(), Color(0), Width(0) {}

	ButterChunkType				Type;
	std::vector<ButterPosition>	Positions;
	uint32_t					Color;
	uint8_t						Width;
};

struct Guess
{
	Guess() : Type(GAttempt), Word() {}

	GuessType	Type;
	std::string	Word;
};

typedef std::vector<ButterChunk> Butter;

struct MessageReceiver
{
	unsigned ProcessBuffer(const char* buffer, unsigned bufferSize, uint8_t senderIndex = 0xFF);

	#ifdef SERVER
	virtual void OnMessageReceivedClientInfo(const ClientInfo& clientInfo, uint8_t senderIndex) = 0;
	virtual void OnMessageReceivedStart(uint8_t senderIndex) = 0;
	#else
	virtual void OnMessageReceivedServerFull() = 0;
	virtual void OnMessageReceivedJoinSuccess(uint8_t myIndex) = 0;
	virtual void OnMessageReceivedClientsData(const ClientsData& clientsData) = 0;
	virtual void OnMessageReceivedPassButter(const PassButter& passButter) = 0;
	virtual void OnMessageReceivedTurnEnd(const std::string& word) = 0;
	#endif
	virtual void OnMessageReceivedButter(const Butter& butter, uint8_t senderIndex) = 0;
	virtual void OnMessageReceivedChat(const std::string& message, uint8_t senderIndex) = 0;
	virtual void OnMessageReceivedGuess(const Guess& guess, uint8_t senderIndex) = 0;
};


// Parse the data in the buffer (should have the full message), and call respective OnMessageReceived callback
unsigned MessageReceiver::ProcessBuffer(const char* buffer, unsigned bufferSize, uint8_t senderIndex)
{
	unsigned offset = 0;
	MessageType messageType = (MessageType)BufferReadUInt8(buffer, bufferSize, offset);

	switch (messageType)
	{
	case MTUndefined:
		{
			ASSERT(false);
		} break;
#ifdef SERVER
	case MTClientInfo:
		{
			ClientInfo clientInfo;
			uint8_t nameLength = BufferReadUInt8(buffer, bufferSize, offset);
			if (nameLength > 0)
			{
				clientInfo.Name.resize(nameLength);
				BufferReadBytes(buffer, bufferSize, offset, &clientInfo.Name[0], nameLength);
			}

			OnMessageReceivedClientInfo(clientInfo, senderIndex);
		} break;
	case MTStart:
		{
			OnMessageReceivedStart(senderIndex);
		} break;
#else
	case MTServerFull:
		{
			OnMessageReceivedServerFull();
		} break;
	case MTJoinSuccess:
		{
			uint8_t myIndex = BufferReadUInt8(buffer, bufferSize, offset);

			OnMessageReceivedJoinSuccess(myIndex);
		} break;
	case MTClientsData:
		{
			ClientsData clientsData;
			uint8_t clientsCount = BufferReadUInt8(buffer, bufferSize, offset);
			for (unsigned i = 0; i < clientsCount; ++i)
			{
				ClientInfo clientInfo;
				uint8_t index = BufferReadUInt8(buffer, bufferSize, offset);
				clientInfo.Points = BufferReadUInt8(buffer, bufferSize, offset);
				uint8_t nameLength = BufferReadUInt8(buffer, bufferSize, offset);
				if (nameLength > 0)
				{
					clientInfo.Name.resize(nameLength);
					BufferReadBytes(buffer, bufferSize, offset, &clientInfo.Name[0], nameLength);
				}

				clientsData.insert(ClientIndexInfo(index, clientInfo));
			}

			OnMessageReceivedClientsData(clientsData);
		} break;
	case MTPassButter:
		{
			PassButter passButter;
			passButter.Type = (PassButterType)BufferReadUInt8(buffer, bufferSize, offset);
			switch (passButter.Type)
			{
			case PBPass:
				{
					passButter.TimeRemaining = BufferReadUInt16(buffer, bufferSize, offset);
					passButter.TurnsLeft = BufferReadUInt8(buffer, bufferSize, offset);
					passButter.DrawingPlayer = BufferReadUInt8(buffer, bufferSize, offset);
				} break;
			case PBMake:
				{
					passButter.TimeRemaining = BufferReadUInt16(buffer, bufferSize, offset);
					passButter.TurnsLeft = BufferReadUInt8(buffer, bufferSize, offset);
					uint8_t wordLength = BufferReadUInt8(buffer, bufferSize, offset);
					if (wordLength > 0)
					{
						passButter.Word.resize(wordLength);
						BufferReadBytes(buffer, bufferSize, offset, &passButter.Word[0], wordLength);
					}
				} break;
			}

			OnMessageReceivedPassButter(passButter);
		} break;
	case MTTurnEnd:
		{
			std::string word;
			uint8_t wordLength = BufferReadUInt8(buffer, bufferSize, offset);
			if (wordLength > 0)
			{
				word.resize(wordLength);
				BufferReadBytes(buffer, bufferSize, offset, &word[0], wordLength);
			}

			OnMessageReceivedTurnEnd(word);
		} break;
#endif
	case MTButter:
		{
			Butter butter;
			uint8_t index = BufferReadUInt8(buffer, bufferSize, offset);
#ifdef SERVER
			index = senderIndex;
#endif
			uint16_t chunkCount = BufferReadUInt16(buffer, bufferSize, offset);
			for (unsigned i = 0; i < chunkCount; ++i)
			{
				ButterChunk butterChunk;
				butterChunk.Type = (ButterChunkType)BufferReadUInt8(buffer, bufferSize, offset);

				switch (butterChunk.Type)
				{
				case BCPosition:
					{
						uint16_t positionCount = BufferReadUInt16(buffer, bufferSize, offset);
						butterChunk.Positions.reserve(positionCount);
						for (unsigned i = 0; i < positionCount; ++i)
						{
							ButterPosition butterPosition;
							butterPosition.X = BufferReadFloat(buffer, bufferSize, offset);
							butterPosition.Y = BufferReadFloat(buffer, bufferSize, offset);
							butterChunk.Positions.push_back(butterPosition);
						}
					} break;
				case BCColor:
					{
						butterChunk.Color = BufferReadUInt32(buffer, bufferSize, offset);
					} break;
				case BCWidth:
					{
						butterChunk.Width = BufferReadUInt8(buffer, bufferSize, offset);
					} break;
				case BCEraser:
					{
					} break;
				case BCClear:
					{
					} break;
				case BCStop:
					{
					} break;
				}

				butter.push_back(butterChunk);
			}

			OnMessageReceivedButter(butter, index);
		} break;
	case MTChat:
		{
			std::string message;
			uint8_t index = BufferReadUInt8(buffer, bufferSize, offset);
#ifdef SERVER
			index = senderIndex;
#endif
			uint16_t messageLength = BufferReadUInt16(buffer, bufferSize, offset);
			if (messageLength > 0)
			{
				message.resize(messageLength);
				BufferReadBytes(buffer, bufferSize, offset, &message[0], messageLength);
			}

			OnMessageReceivedChat(message, index);
		} break;
	case MTGuess:
		{
			Guess guess;
			guess.Type = (GuessType)BufferReadUInt8(buffer, bufferSize, offset);
			uint8_t index = 0;
			switch (guess.Type)
			{
			case GAttempt:
				{
					index = BufferReadUInt8(buffer, bufferSize, offset);
#ifdef SERVER
					index = senderIndex;
#endif
				} break;
			case GSuccess:
				{
				} break;
			}
			uint8_t wordLength = BufferReadUInt8(buffer, bufferSize, offset);
			if (wordLength > 0)
			{
				guess.Word.resize(wordLength);
				BufferReadBytes(buffer, bufferSize, offset, &guess.Word[0], wordLength);
			}

			OnMessageReceivedGuess(guess, index);
		} break;
	}

	return offset;
}

#ifdef SERVER
unsigned GenerateMessageServerFull(char* buffer, unsigned bufferSize)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTServerFull);

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageJoinSuccess(char* buffer, unsigned bufferSize, uint8_t clientIndex)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTJoinSuccess);
	BufferWriteUInt8(buffer, bufferSize, offset, clientIndex);

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageClientsData(char* buffer, unsigned bufferSize, const ClientsData& clientsData)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTClientsData);
	BufferWriteUInt8(buffer, bufferSize, offset, (uint8_t)clientsData.size());

	for (auto& kv : clientsData)
	{
		BufferWriteUInt8(buffer, bufferSize, offset, kv.first);

		const ClientInfo& clientInfo = kv.second;
		BufferWriteUInt8(buffer, bufferSize, offset, clientInfo.Points);
		uint8_t nameLength = clientInfo.Name.length();
		BufferWriteUInt8(buffer, bufferSize, offset, nameLength);
		if (nameLength > 0)
		{
			BufferWriteBytes(buffer, bufferSize, offset, &clientInfo.Name[0], nameLength);
		}
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessagePassButter(char* buffer, unsigned bufferSize, const PassButter& passButter)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTPassButter);
	BufferWriteUInt8(buffer, bufferSize, offset, passButter.Type);

	switch (passButter.Type)
	{
	case PBPass:
		{
			BufferWriteUInt16(buffer, bufferSize, offset, passButter.TimeRemaining);
			BufferWriteUInt8(buffer, bufferSize, offset, passButter.TurnsLeft);
			BufferWriteUInt8(buffer, bufferSize, offset, passButter.DrawingPlayer);
		} break;
	case PBMake:
		{
			BufferWriteUInt16(buffer, bufferSize, offset, passButter.TimeRemaining);
			BufferWriteUInt8(buffer, bufferSize, offset, passButter.TurnsLeft);
			uint8_t wordLength = passButter.Word.length();
			BufferWriteUInt8(buffer, bufferSize, offset, wordLength);
			if (wordLength > 0)
			{
				BufferWriteBytes(buffer, bufferSize, offset, &passButter.Word[0], wordLength);
			}
		} break;
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageTurnEnd(char* buffer, unsigned bufferSize, const std::string word)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTTurnEnd);
	uint8_t wordLength = word.length();
	BufferWriteUInt8(buffer, bufferSize, offset, wordLength);
	if (wordLength > 0)
	{
		BufferWriteBytes(buffer, bufferSize, offset, &word[0], wordLength);
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}
#else
unsigned GenerateMessageClientInfo(char* buffer, unsigned bufferSize, const ClientInfo& clientInfo)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTClientInfo);
	uint8_t nameLength = clientInfo.Name.length();
	BufferWriteUInt8(buffer, bufferSize, offset, nameLength);
	if (nameLength > 0)
	{
		BufferWriteBytes(buffer, bufferSize, offset, &clientInfo.Name[0], nameLength);
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageStart(char* buffer, unsigned bufferSize)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTStart);

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}
#endif

unsigned GenerateMessageButter(char* buffer, unsigned bufferSize, const Butter& butter, uint8_t senderIndex)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTButter);
	BufferWriteUInt8(buffer, bufferSize, offset, senderIndex);
	uint16_t chunkCount = butter.size();
	BufferWriteUInt16(buffer, bufferSize, offset, chunkCount);
	for (int i = 0; i < chunkCount; ++i)
	{
		const ButterChunk& butterChunk = butter[i];
		BufferWriteUInt8(buffer, bufferSize, offset, butterChunk.Type);

		switch (butterChunk.Type)
		{
		case BCPosition:
			{
				uint16_t positionCount = butterChunk.Positions.size();
				BufferWriteUInt16(buffer, bufferSize, offset, positionCount);

				for (int j = 0; j < positionCount; ++j)
				{
					const ButterPosition& butterPosition = butterChunk.Positions[j];

					BufferWriteFloat(buffer, bufferSize, offset, butterPosition.X);
					BufferWriteFloat(buffer, bufferSize, offset, butterPosition.Y);
				}
			} break;
		case BCColor:
			{
				BufferWriteUInt32(buffer, bufferSize, offset, butterChunk.Color);
			} break;
		case BCWidth:
			{
				BufferWriteUInt8(buffer, bufferSize, offset, butterChunk.Width);
			} break;
		case BCEraser:
			{
			} break;
		case BCClear:
			{
			} break;
		case BCStop:
			{
			} break;
		}
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageChat(char* buffer, unsigned bufferSize, const std::string& message, uint8_t senderIndex = 0xFF)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTChat);
	BufferWriteUInt8(buffer, bufferSize, offset, senderIndex);

	uint16_t messageLength = message.length();
	BufferWriteUInt16(buffer, bufferSize, offset, messageLength);
	if (messageLength > 0)
	{
		BufferWriteBytes(buffer, bufferSize, offset, &message[0], messageLength);
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}

unsigned GenerateMessageGuess(char* buffer, unsigned bufferSize, const Guess& guess, uint8_t senderIndex = 0xFF)
{
	unsigned offset = 2; // First 2B are for the message size

	BufferWriteUInt8(buffer, bufferSize, offset, MTGuess);
	BufferWriteUInt8(buffer, bufferSize, offset, guess.Type);
	switch (guess.Type)
	{
	case GAttempt:
		{
			BufferWriteUInt8(buffer, bufferSize, offset, senderIndex);
		} break;
	case GSuccess:
		{
		} break;
	}
	uint8_t wordLength = guess.Word.length();
	BufferWriteUInt8(buffer, bufferSize, offset, wordLength);
	if (wordLength > 0)
	{
		BufferWriteBytes(buffer, bufferSize, offset, &guess.Word[0], wordLength);
	}

	uint16_t messageSize = offset - 2;
	unsigned offsetStart = 0;
	BufferWriteUInt16(buffer, bufferSize, offsetStart, messageSize);

	return offset;
}