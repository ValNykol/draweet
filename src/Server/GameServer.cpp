#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

#include "SDL.h"
#include "SDL_net.h"

#define SERVER

#include "Core/MacroUtils.h"
#include "Core/NetworkHelper.h"

static const unsigned MAX_SOCKETS = 17;
static const unsigned MAX_CLIENTS = MAX_SOCKETS - 1;
static const unsigned SLEEP_DURATION = 30;

static const unsigned CHAT_MEMORY = 20;
static const unsigned GUESS_MEMORY = 10;

struct WordEntry
{
	std::vector<std::string>	Variants;	// Variants[0] is shown to the player. Word is "guessed" if it matches any of the variants
	// unsigned					Weight;		// Make "barnacle" and "posh" more likely to appear
};

typedef std::vector<WordEntry> Dictionary;

struct ClientConnection
{
	TCPsocket	Socket;
	bool		Taken;

	ClientInfo	Info;
	bool		GuessedThisTurn;

	char		InBuffer[BUFFER_SIZE];
	unsigned	InBufferOffset;
	uint16_t	InMessageSize;	// Including the 2B for the message size itself
	char		OutBuffer[BUFFER_SIZE];
	unsigned	OutBufferOffset;
};

struct GameServer : MessageReceiver
{
	bool Init();
	void Update();
	void StartTheGame();
	void NextTurn();

	void CopyMessage(const char* buffer, unsigned messageSize, uint8_t receiverIndex);

	// MessageReceiver
	virtual void OnMessageReceivedClientInfo(const ClientInfo& clientInfo, uint8_t senderIndex);
	virtual void OnMessageReceivedStart(uint8_t senderIndex);

	virtual void OnMessageReceivedButter(const Butter& butter, uint8_t senderIndex);
	virtual void OnMessageReceivedChat(const std::string& message, uint8_t senderIndex);
	virtual void OnMessageReceivedGuess(const Guess& guess, uint8_t senderIndex);

	bool						m_Running;
	TCPsocket					m_ServerSocket;
	SDLNet_SocketSet			m_SocketSet;

	ClientConnection			m_Connections[MAX_CLIENTS];
	uint8_t						m_ConnectionCount;

	Dictionary					m_Dictionary;
	unsigned					m_CurrentWord;
	uint8_t						m_DrawingPlayer;
	uint8_t						m_GuessCount;
	uint8_t						m_CurrentTurn;
	bool						m_GameStarted;
	std::vector<unsigned>		m_WordOrder;

	uint8_t						m_PlayersToStart;
	uint8_t						m_TotalTurns;
	uint16_t					m_TurnDuration;
	unsigned					m_TurnStart;

	Butter						m_Butter;

	typedef std::pair<uint8_t, std::string> ChatMessage;
	std::vector<ChatMessage>	m_Chat;

	typedef std::pair<uint8_t, std::string> GuessMessage;
	std::vector<GuessMessage>	m_Guesses;

	char						m_PacketBuffer[PACKET_SIZE];
};

static GameServer m_GameServer;

void GameServer::OnMessageReceivedClientInfo(const ClientInfo& clientInfo, uint8_t senderIndex)
{
	AssertReturnUnless(m_Connections[senderIndex].Taken);
	
	m_Connections[senderIndex].Info = clientInfo;

	ClientsData clientsData;
	clientsData.insert(ClientIndexInfo(senderIndex, clientInfo));

	printf("%d [%s] has joined the game.\n", senderIndex, clientInfo.Name.c_str());

	unsigned messageSize = GenerateMessageClientsData(m_PacketBuffer, sizeof(m_PacketBuffer), clientsData);

	for (unsigned index = 0; index < MAX_CLIENTS; ++index)
	{
		ContinueIf(index == senderIndex);
		CopyMessage(m_PacketBuffer, messageSize, index);
	}
}

void GameServer::OnMessageReceivedStart(uint8_t senderIndex)
{
	ReturnIf(m_GameStarted);
	StartTheGame();
}

void GameServer::OnMessageReceivedButter(const Butter& butter, uint8_t senderIndex)
{
	AssertReturnUnless(m_Connections[senderIndex].Taken);
	ReturnUnless(m_GameStarted);
	ReturnUnless(senderIndex == m_DrawingPlayer);

	m_Butter.insert(m_Butter.end(), butter.begin(), butter.end());

	//printf("Got butter from %d.\n", senderIndex);

	unsigned bufferSize = BUFFER_SIZE;
	char* buffer = (char*)malloc(bufferSize);

	unsigned messageSize = GenerateMessageButter(buffer, bufferSize, butter, senderIndex);

	for (unsigned index = 0; index < MAX_CLIENTS; ++index)
	{
		ContinueIf(index == senderIndex);
		CopyMessage(buffer, messageSize, index);
	}

	free(buffer);
}

void GameServer::OnMessageReceivedChat(const std::string& message, uint8_t senderIndex)
{
	AssertReturnUnless(m_Connections[senderIndex].Taken);

	m_Chat.push_back(GameServer::ChatMessage(senderIndex, message));
	if (m_Chat.size() > CHAT_MEMORY)
	{
		m_Chat.erase(m_Chat.begin());
	}

	printf("Client %d says %s\n", senderIndex, message.c_str());

	unsigned bufferSize = BUFFER_SIZE;
	char* buffer = (char*)malloc(bufferSize);

	unsigned messageSize = GenerateMessageChat(buffer, bufferSize, message, senderIndex);

	for (unsigned index = 0; index < MAX_CLIENTS; ++index)
	{
		ContinueIf(index == senderIndex);
		CopyMessage(buffer, messageSize, index);
	}

	free(buffer);
}

void GameServer::OnMessageReceivedGuess(const Guess& guess, uint8_t senderIndex)
{
	AssertReturnUnless(m_Connections[senderIndex].Taken);
	ReturnUnless(m_GameStarted);
	ReturnIf(m_Connections[senderIndex].GuessedThisTurn);

	std::string lowercaseGuess = guess.Word;
	std::transform(lowercaseGuess.begin(), lowercaseGuess.end(), lowercaseGuess.begin(), ::tolower);

	printf("Guess attempt by %d: %s\n", senderIndex, lowercaseGuess.c_str());

	bool correctGuess = false;
	WordEntry& currentWordEntry = m_Dictionary[m_CurrentWord];
	for (auto& variant : currentWordEntry.Variants)
	{
		if (lowercaseGuess == variant)
		{
			correctGuess = true;
			break;
		}
	}

	if (correctGuess)
	{
		ClientConnection& senderConnection = m_Connections[senderIndex];
		{
			// Send GSuccess and original word variant to the sender
			Guess guessResponse;
			guessResponse.Type = GSuccess;
			guessResponse.Word = currentWordEntry.Variants[0];

			char* senderBuffer = senderConnection.OutBuffer + senderConnection.OutBufferOffset;
			unsigned senderBufferSize = sizeof(senderConnection.OutBuffer) - senderConnection.OutBufferOffset;
			senderConnection.OutBufferOffset += GenerateMessageGuess(senderBuffer, senderBufferSize, guessResponse);
		}

		// Add points to sender and drawing player
		ClientConnection& drawingConnection = m_Connections[m_DrawingPlayer];
		AssertReturnUnless(drawingConnection.Taken);

		senderConnection.Info.Points += (m_GuessCount == 0) ? 3 : 2;
		senderConnection.GuessedThisTurn = true;
		drawingConnection.Info.Points += 1;

		++m_GuessCount;
		
		{
			// Send information about the points to everyone
			ClientsData clientsData;
			
			ClientInfo senderInfo;
			senderInfo.Points = senderConnection.Info.Points;
			clientsData.insert(ClientIndexInfo(senderIndex, senderInfo));

			ClientInfo drawingPlayerInfo;
			drawingPlayerInfo.Points = drawingConnection.Info.Points;
			clientsData.insert(ClientIndexInfo(m_DrawingPlayer, drawingPlayerInfo));

			unsigned messageSize = GenerateMessageClientsData(m_PacketBuffer, sizeof(m_PacketBuffer), clientsData);

			for (unsigned index = 0; index < MAX_CLIENTS; ++index)
			{
				CopyMessage(m_PacketBuffer, messageSize, index);
			}
		}

		if (m_GuessCount == m_ConnectionCount - 1)
		{
			NextTurn();
		}
	}
	else
	{
		// Failed guess, send to everyone
		unsigned messageSize = GenerateMessageGuess(m_PacketBuffer, sizeof(m_PacketBuffer), guess, senderIndex);

		for (unsigned index = 0; index < MAX_CLIENTS; ++index)
		{
			CopyMessage(m_PacketBuffer, messageSize, index);
		}
	}
}

bool GameServer::Init()
{
	// m_PlayersToStart = 5;
	// m_TotalTurns = 20;

	printf("Players to automatically start: ");
	scanf("%d", &m_PlayersToStart);

	printf("Number of turns (total): ");
	scanf("%d", &m_TotalTurns);

	printf("Turn duration (seconds): ");
	scanf("%d", &m_TurnDuration);

	FILE* dictionaryFile = fopen("dictionary.txt", "r");
	if (!dictionaryFile)
	{
		fprintf(stderr, "Could not find dictionary.txt!");
		return false;
	}

	unsigned wordCount;
	fscanf(dictionaryFile, "%d", &wordCount);

	// Read till the end of the line
	fgets(m_PacketBuffer, sizeof(m_PacketBuffer), dictionaryFile);

	if (wordCount < m_TotalTurns)
	{
		fprintf(stderr, "There are not enough words in the dictionary!");
		return false;
	}

	if (wordCount >= 10000)
	{
		fprintf(stderr, "The dictionary should begin with the word count. It should be less than 10,000.");
		return false;
	}

	{
		char wordBuffer[PACKET_SIZE];

		m_Dictionary.resize(wordCount);

		for (unsigned word = 0; word < wordCount; ++word)
		{
			fgets(wordBuffer, sizeof(wordBuffer), dictionaryFile);

			unsigned i = 0;
			unsigned variantStart = 0;
			while (wordBuffer[i])
			{
				if (wordBuffer[i] == ',' || wordBuffer[i] == '\n')
				{
					m_Dictionary[word].Variants.push_back(std::string(wordBuffer + variantStart, wordBuffer + i));
					std::string& variant = m_Dictionary[word].Variants.back();
					std::transform(variant.begin(), variant.end(), variant.begin(), ::tolower);
					variantStart = i + 1;
				}
				++i;
			}

			if (m_Dictionary[word].Variants.empty())
			{
				fprintf(stderr, "Dictionary looks a bit broken at line: %d\n", word + 1);
				m_Dictionary[word].Variants.push_back("i am error");
			}
		}
	}

	fclose(dictionaryFile);

	IPaddress serverIP;

	if (SDL_Init(SDL_INIT_TIMER) < 0)
	{
		fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
		return false;
	}

	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		return false;
	}

	if (SDLNet_ResolveHost(&serverIP, 0, SERVER_PORT) < 0)
	{
		fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		return false;
	}

	if (!(m_ServerSocket = SDLNet_TCP_Open(&serverIP)))
	{
		fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		return false;
	}

	{
		uint8_t* address = (uint8_t*)&serverIP.host;
		printf("Server IP: %d.%d.%d.%d\n", address[0], address[1], address[2], address[3]);
	}

	const char* hostName = SDLNet_ResolveIP(&serverIP);
	if (hostName)
	{
		printf("Server host name: %s\n", hostName);
	}

	m_SocketSet = SDLNet_AllocSocketSet(MAX_SOCKETS);
	SDLNet_TCP_AddSocket(m_SocketSet, m_ServerSocket);

	m_Running = true;
	m_GameStarted = false;

	return true;
}

void GameServer::CopyMessage(const char* buffer, unsigned messageSize, uint8_t receiverIndex)
{
	ClientConnection& connection = m_Connections[receiverIndex];
	ReturnUnless(connection.Taken);
	AssertReturnIf(connection.OutBufferOffset + messageSize > sizeof(connection.OutBuffer));

	memcpy((void*)(connection.OutBuffer + connection.OutBufferOffset), (void*)buffer, messageSize);
	connection.OutBufferOffset += messageSize;
}

void GameServer::StartTheGame()
{
	printf("The game is starting!\n");

	srand((unsigned)time(0));

	m_GameStarted = true;
	m_DrawingPlayer = MAX_CLIENTS - 1;

	m_CurrentTurn = 0;
	m_Butter.clear();
	m_Guesses.clear();

	m_WordOrder.resize(m_Dictionary.size());

	for (unsigned i = 0; i < m_WordOrder.size(); ++i)
	{
		m_WordOrder[i] = i;
	}

	for (unsigned i = 0; i < m_WordOrder.size(); ++i)
	{
		unsigned target = rand() % m_WordOrder.size();
		std::swap(m_WordOrder[i], m_WordOrder[target]);
	}

	NextTurn();
}

void GameServer::NextTurn()
{
	ReturnIf(m_DrawingPlayer == 0xFF);

	if (m_ConnectionCount == 0)
	{
		m_Running = false;
		return;
	}

	if (m_CurrentTurn > 0)
	{
		unsigned messageSize = GenerateMessageTurnEnd(m_PacketBuffer, sizeof(m_PacketBuffer), m_Dictionary[m_CurrentWord].Variants[0]);

		for (unsigned index = 0; index < MAX_CLIENTS; ++index)
		{
			CopyMessage(m_PacketBuffer, messageSize, index);
		}
	}

	do
	{
		++m_DrawingPlayer;
		if (m_DrawingPlayer >= MAX_CLIENTS)
		{
			m_DrawingPlayer = 0;
		}
	}
	while (!m_Connections[m_DrawingPlayer].Taken);

	++m_CurrentTurn;

	if (m_CurrentTurn > m_TotalTurns)
	{
		m_DrawingPlayer = 0xFF;

		printf("\nThe game is kill!\n");
	}
	else
	{
		printf("\nTurn %d\n", m_CurrentTurn);

		m_CurrentWord = m_WordOrder[m_CurrentTurn - 1];
	}

	m_TurnStart = SDL_GetTicks() / 1000;

	m_GuessCount = 0;
	m_Butter.clear();
	m_Guesses.clear();

	{
		PassButter passButter;
		passButter.Type = PBPass;
		passButter.TimeRemaining = m_TurnDuration;
		passButter.TurnsLeft = m_TotalTurns - m_CurrentTurn;
		passButter.DrawingPlayer = m_DrawingPlayer;

		unsigned messageSize = GenerateMessagePassButter(m_PacketBuffer, sizeof(m_PacketBuffer), passButter);

		for (unsigned index = 0; index < MAX_CLIENTS; ++index)
		{
			ClientConnection& connection = m_Connections[index];
			ContinueUnless(connection.Taken);

			connection.GuessedThisTurn = false;

			if (index == m_DrawingPlayer)
			{
				PassButter makeButter = passButter;
				makeButter.Type = PBMake;
				makeButter.Word = m_Dictionary[m_CurrentWord].Variants[0];

				char* drawingPlayerBuffer = connection.OutBuffer + connection.OutBufferOffset;
				unsigned drawingPlayerBufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
				connection.OutBufferOffset += GenerateMessagePassButter(drawingPlayerBuffer, drawingPlayerBufferSize, makeButter);
			}
			else
			{
				CopyMessage(m_PacketBuffer, messageSize, index);
			}
		}
	}
}

void GameServer::Update()
{
	if (SDLNet_CheckSockets(m_SocketSet, SLEEP_DURATION))
	{
		if (SDLNet_SocketReady(m_ServerSocket))
		{
			// Got a new client
			if (m_ConnectionCount < MAX_CLIENTS)
			{
				// Got space?
				int newClientIndex = 0;
				for (; newClientIndex < MAX_CLIENTS; ++newClientIndex)
				{
					ClientConnection& connection = m_Connections[newClientIndex];
					if (!connection.Taken)
					{
						// Found it!
						connection.Taken = true;

						connection.InBufferOffset = 0;
						connection.InMessageSize = 0;
						connection.Info.Name = "";
						connection.Info.Points = 0;
						connection.OutBufferOffset = 0;

						connection.Socket = SDLNet_TCP_Accept(m_ServerSocket);
						SDLNet_TCP_AddSocket(m_SocketSet, connection.Socket);
						++m_ConnectionCount;

						{
							printf("\nGot a new client! Client index: %d\n", newClientIndex);

							IPaddress* clientAddress = SDLNet_TCP_GetPeerAddress(connection.Socket);
							if (clientAddress)
							{
								uint8_t* address = (uint8_t*)&clientAddress->host;
								printf("Client IP: %d.%d.%d.%d\n", address[0], address[1], address[2], address[3]);
							}
							else
							{
								printf("... buuut it doesn't have an IP? O_o\n");
							}
						}

						connection.OutBufferOffset = GenerateMessageJoinSuccess(connection.OutBuffer, sizeof(connection.OutBuffer), newClientIndex);

						if (m_ConnectionCount > 1)
						{
							ClientsData clientsData;
							for (unsigned index = 0; index < MAX_CLIENTS - 1; ++index)
							{
								ContinueIf(!m_Connections[index].Taken ||
										   index == newClientIndex ||
										   m_Connections[index].Info.Name.empty());

								clientsData.insert(ClientIndexInfo(index, m_Connections[index].Info));
							}

							char* buffer = connection.OutBuffer + connection.OutBufferOffset;
							unsigned bufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
							connection.OutBufferOffset += GenerateMessageClientsData(buffer, bufferSize, clientsData);
						}

						for (auto& chatMessage : m_Chat)
						{
							char* buffer = connection.OutBuffer + connection.OutBufferOffset;
							unsigned bufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
							connection.OutBufferOffset += GenerateMessageChat(buffer, bufferSize, chatMessage.second, chatMessage.first);
						}

						if (m_GameStarted)
						{
							{
								PassButter passButter;
								passButter.Type = PBPass;
								passButter.TimeRemaining = m_TurnDuration - (SDL_GetTicks() / 1000 - m_TurnStart);
								passButter.TurnsLeft = m_TotalTurns - m_CurrentTurn;
								passButter.DrawingPlayer = m_DrawingPlayer;

								char* buffer = connection.OutBuffer + connection.OutBufferOffset;
								unsigned bufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
								connection.OutBufferOffset += GenerateMessagePassButter(buffer, bufferSize, passButter);
							}

							for (auto& guessMessage : m_Guesses)
							{
								Guess guess;
								guess.Type = GAttempt;
								guess.Word = guessMessage.second;

								char* buffer = connection.OutBuffer + connection.OutBufferOffset;
								unsigned bufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
								connection.OutBufferOffset += GenerateMessageGuess(buffer, bufferSize, guess, guessMessage.first);
							}

							if (!m_Butter.empty())
							{
								char* buffer = connection.OutBuffer + connection.OutBufferOffset;
								unsigned bufferSize = sizeof(connection.OutBuffer) - connection.OutBufferOffset;
								connection.OutBufferOffset += GenerateMessageButter(buffer, bufferSize, m_Butter, m_DrawingPlayer);
							}
						}

						// TODO: Start the game if enough players
						if (m_ConnectionCount >= m_PlayersToStart && !m_GameStarted)
						{
							StartTheGame();
						}

						break;
					}
				}
			}
			else
			{
				// No space :/
				TCPsocket tempSocket = SDLNet_TCP_Accept(m_ServerSocket);

				{
					printf("\nGot a new client, but we're already full...\n");

					IPaddress* clientAddress = SDLNet_TCP_GetPeerAddress(tempSocket);
					if (clientAddress)
					{
						uint8_t* address = (uint8_t*)&clientAddress->host;
						printf("Client IP: %d.%d.%d.%d\n", address[0], address[1], address[2], address[3]);
					}
					else
					{
						printf("... buuut it doesn't have an IP? O_o\n");
					}
				}

				unsigned messageSize = GenerateMessageServerFull(m_PacketBuffer, sizeof(m_PacketBuffer));
				SDLNet_TCP_Send(tempSocket, (void*)m_PacketBuffer, messageSize);
				SDLNet_TCP_Close(tempSocket);
			}
		}

		for (unsigned clientIndex = 0; clientIndex < MAX_CLIENTS; ++clientIndex)
		{
			ClientConnection& connection = m_Connections[clientIndex];
			ContinueUnless(connection.Taken);

			while (SDLNet_SocketReady(connection.Socket))
			{
				int receivedBytes = SDLNet_TCP_Recv(connection.Socket, (void*)m_PacketBuffer, sizeof(m_PacketBuffer));

				if (receivedBytes <= 0)
				{
					// Bye-bye
					printf("\nFarewell, client %d! You will be missed.\n", clientIndex);

					SDLNet_TCP_DelSocket(m_SocketSet, connection.Socket);
					SDLNet_TCP_Close(connection.Socket);

					connection.Socket = 0;
					connection.Taken = false;

					--m_ConnectionCount;

					if (m_GameStarted)
					{
						if (connection.GuessedThisTurn)
						{
							--m_GuessCount;
						}

						if (m_DrawingPlayer == clientIndex || m_GuessCount == m_ConnectionCount - 1)
						{
							NextTurn();
						}
					}

					ClientsData clientsData;
					ClientInfo clientInfo;
					clientsData.insert(ClientIndexInfo(clientIndex, clientInfo));

					unsigned messageSize = GenerateMessageClientsData(m_PacketBuffer, sizeof(m_PacketBuffer), clientsData);

					for (unsigned index = 0; index < MAX_CLIENTS; ++index)
					{
						CopyMessage(m_PacketBuffer, messageSize, index);
					}
				}
				else
				{
					AssertBreakIf(connection.InBufferOffset + receivedBytes > sizeof(connection.InBuffer));

					memcpy((void*)(connection.InBuffer + connection.InBufferOffset), (void*)m_PacketBuffer, receivedBytes);
					connection.InBufferOffset += receivedBytes;

					while (connection.InBufferOffset > 0 && connection.InBufferOffset >= connection.InMessageSize)
					{
						if (connection.InMessageSize > 0)
						{
							unsigned messageSize = ProcessBuffer((connection.InBuffer + 2), sizeof(connection.InBuffer) - 2, clientIndex) + 2;
							ASSERT(messageSize == connection.InMessageSize);

							connection.InMessageSize = 0;
							connection.InBufferOffset -= messageSize;
							memmove((void*)connection.InBuffer, (void*)(connection.InBuffer + messageSize), connection.InBufferOffset);
						}

						if (connection.InBufferOffset >= 2)
						{
							unsigned offsetStart = 0;
							connection.InMessageSize = BufferReadUInt16(connection.InBuffer, sizeof(connection.InBuffer), offsetStart) + 2;
						}
					}
				}
			}
		}
	}

	if (m_GameStarted && m_DrawingPlayer < 0xFF)
	{
		unsigned now = SDL_GetTicks() / 1000;

		if (now >= m_TurnStart + m_TurnDuration)
		{
			NextTurn();
		}
	}

	for (unsigned i = 0; i < MAX_CLIENTS; ++i)
	{
		ClientConnection& connection = m_Connections[i];
		ContinueUnless(connection.Taken);

		unsigned bytesToSend = PACKET_SIZE;
		if (connection.OutBufferOffset < bytesToSend)
		{
			bytesToSend = connection.OutBufferOffset;
		}

		if (bytesToSend > 0)
		{
			int bytesSent = SDLNet_TCP_Send(connection.Socket, (void*)connection.OutBuffer, bytesToSend);

			if (bytesSent < (int)bytesToSend)
			{
				// TODO: Handle send error
				ASSERT(false);
			}

			connection.OutBufferOffset -= bytesSent;
			memmove((void*)connection.OutBuffer, (void*)(connection.OutBuffer + bytesSent), connection.OutBufferOffset);
		}
	}
}

int SDL_main(int argc, char* argv[])
{
	if (!m_GameServer.Init())
	{
		exit(EXIT_FAILURE);
	}

	while (m_GameServer.m_Running)
	{
		m_GameServer.Update();
	}

	return 0;
}