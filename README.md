# README #

This is an online multiplayer Pictionary game. Written in C++ using SDL2 and CEGUI.

### How do I download it? ###

* Go to Source
* There are 3 downloads available:
* DrawEet.zip - this is client and server bundled together
* DrawEetBeta-install.exe - installer with only client (this is the one you want your friends to get)
* DrawEetServer.zip - archive with only server
* You need to have Microsoft Visual C++ 2012 Redistributable (x86) to run the game (it comes bundled with the client in redist folder)

### How do I start the game? ###

* One of the players must host the game. (If that's you, go to "How do I host?" section)
* Other players type in the host's public IP and connect
* Once there are players in the lobby, typing /start will force the game start

### How do I host? ###

* First, you need add a forwarding rule for port 1337 on your router (the steps may vary, so better look this step up online)
* Now you can start DrawEetServer.exe
* Type in the number of players for auto-start, number of turns and turn duration
* Now the players should be able to join

### How do I set up the project? ###

* Inside _CEGUI folder, unpack the archives
* Use CMake on cegui-deps-0.8.x-src with target cegui-deps
* Open the CEGUI-DEPS solution and build for Debug and Release
* Copy the dependencies folder to cegui-0.8.7
* Use CMake on cegui-0.8.7 with target cegui-0.8.7-build (you can exclude all the samples during the configuration step)
* Open the cegui solution and build for Debug and Release
* Copy the 3 files from cegui-0.8.7-build\cegui\include\CEGUI to cegui-0.8.7\cegui\include\CEGUI
* Copy all the *.dll files from dependencies\bin and cegui-0.8.7-build\bin to DrawEet\build
* Now you should be ready to compile the game on your computer

### Can I use the source code? ###

* Sure, as long as you give me credit for it.

*Valentyn Nykoliuk*